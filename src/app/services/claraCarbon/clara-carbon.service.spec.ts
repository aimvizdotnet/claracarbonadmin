import { TestBed } from '@angular/core/testing';
import { ClaraCarbonService } from './clara-carbon.service';

describe('ClaraCarbonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClaraCarbonService = TestBed.get(ClaraCarbonService);
    expect(service).toBeTruthy();
  });
});
