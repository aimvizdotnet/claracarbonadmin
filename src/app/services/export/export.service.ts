import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ExportService {
  url: string = environment.server;
  constructor(private http: HttpClient) {

  }
  async getAllTrainigRecords(deptId) {
    return await this.http.get<any[]>(this.url + 'Training/getAllTrainigRecords?deptId=' + deptId).toPromise();
  }

}
