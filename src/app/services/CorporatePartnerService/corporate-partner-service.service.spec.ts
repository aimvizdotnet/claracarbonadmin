import { TestBed } from '@angular/core/testing';

import { CorporatePartnerServiceService } from './corporate-partner-service.service';

describe('CorporatePartnerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CorporatePartnerServiceService = TestBed.get(CorporatePartnerServiceService);
    expect(service).toBeTruthy();
  });
});
