import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CorporatePartnerServiceService {

  url: string = environment.server;

  constructor(private http:HttpClient) {

   }

   CreateCorporatePartner(obj:any){
     return this.http.post(`${this.url}CreateCorporatePartner`,obj);

   }

   getCorporatePartnerById(Id){

    return this.http.get(`${this.url}getCorporatePartnerById/${Id}`);
  
  }

  getCorporatePartners(limit:number,page:number,filter:string){

    return this.http.get(`${this.url}CorporatePartner?limit=${limit}&page=${page}`+(filter?`&CorporatePartnerName=${filter}`:''));
  
  }
  deleteCorporatePartner(Id){

    return this.http.get(`${this.url}DeleteCorporatePartner/${Id}`);

  }
  UpdateCorporatePartner(data){

    return this.http.post(`${this.url}UpdateCorporatePartner`,JSON.stringify(data));
  }
}
