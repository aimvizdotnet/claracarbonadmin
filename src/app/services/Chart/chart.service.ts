import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  url: string = environment.server;
  constructor(private http:HttpClient) { }
  GetOffsetsStatsForAdmin(dateFilter){
    return this.http.post<any>(`${this.url}GetOffsetsStatsForAdmin`,dateFilter);
  }
}
