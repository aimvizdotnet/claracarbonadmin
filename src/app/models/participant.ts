export class Participant{
    ParticipantId: number;
    firstName: string;
    cnic: string;
    gender: string
    dob: string
    qualification: string;
    designation: string;
    experience: string;
    phone: string;
    email: string;
    placeOfPosting: string;
    pretestScore: number;
    posttestScore: number;
    comment: string;
    createdDate: string;
    deleted: boolean
}