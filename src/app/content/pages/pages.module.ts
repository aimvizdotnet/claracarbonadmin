import { LayoutModule } from '../layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { PartialsModule } from '../partials/partials.module';
import { ActionComponent } from './header/action/action.component';
import { ProfileComponent } from './header/profile/profile.component';
import { CoreModule } from '../../core/core.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from './components/inner/inner.component';
import { TrainingComponent } from './components/training/listTraining/training.component';
import { LoaderComponent } from './components/loader/loader.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddTrainingComponent } from './components/training/add-training/add-training.component';
import { BsDatepickerModule, BsDatepickerConfig } from 'ngx-bootstrap';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import {
	MatCheckboxModule, MatAutocompleteModule, MatButtonToggleModule, MatCardModule, MatChipsModule,
	MatDatepickerModule,
	MatDialogModule,
	MatDividerModule,
	MatExpansionModule,
	MatFormFieldModule,
	MatGridListModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatProgressBarModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatRippleModule,
	MatSelectModule,
	MatSidenavModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatSortModule,
	MatStepperModule,
	MatTableModule,
	MatTabsModule,
	MatToolbarModule,
	MatTooltipModule, MatButtonModule,
} from '@angular/material';

import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DetailTrainingComponent } from './components/training/detail-training/detail-training.component';
import { AddTrainingCategoryComponent } from './components/trainingCategory/add-training-category/add-training-category.component';
import { AddOrganizationComponent } from './components/organization/add-organization/add-organization.component';
import { AddDistrictComponent } from './components/district/add-district/add-district.component';
import { TrainedParticipantByCNICnameComponent } from './components/Reports/trained-participant-by-cnicname/trained-participant-by-cnicname.component';
import { TrainingSummarybydistrictComponent } from './components/Reports/training-summarybydistrict/training-summarybydistrict.component';
import { TrainingdetailreportbyfacilityComponent } from './components/Reports/trainingdetailreportbyfacility/trainingdetailreportbyfacility.component';
import { AddDepartmentComponent } from './components/department/add-department/add-department.component';
import { AddPlaceOfPostingComponent } from './components/placeOfPosting/add-place-of-posting/add-place-of-posting.component';
import { ChartModule } from 'angular2-chartjs';
import { AddScoreComponent } from './components/scores/add-score/add-score.component';
import { AddProgramComponent } from './components/program/add-program/add-program.component';
import { AddPartnerComponent } from './components/partner/add-partner/add-partner.component';
import { AddTehseelComponent } from './components/tehseel/add-tehseel/add-tehseel.component';
import { ChartsModule } from 'ng2-charts';
import { HomeComponent } from './components/home/home.component';
import { NgxMaskModule } from 'ngx-mask-2';


import { AddJobTitleComponent } from './components/jobTitle/add-job-title/add-job-title.component';

import { AddProgramOtherModalComponent } from './components/training/add-program-other-modal/add-program-other-modal.component';

import { SupportedByComponent } from './components/supported-by/supported-by.component';
import { AddTrainingModuleComponent } from './components/trainingCategory/add-training-module/add-training-module.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ProductComponent } from './components/product/product.component';
import { ProductModalComponent } from './components/product/product-modal/product-modal.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { LineComponent } from './components/charts/line/line.component';
import { RadarComponent } from './components/charts/radar/radar.component';
import { PieComponent } from './components/charts/pie/pie.component';
import { BarComponent } from './components/charts/bar/bar.component';
import { CorporatePartnerComponent } from './components/corporate-partner/corporate-partner.component';
import { CorporatePartnerModalComponent } from './components/corporate-partner/corporate-partner-modal/corporate-partner-modal.component';








export function getDatepickerConfig(): BsDatepickerConfig {
	return Object.assign(new BsDatepickerConfig(), {
		dateInputFormat: 'MMM DD,YYYY',
		
	});
}
//export const options: Partial<IConfig> | (() => Partial<IConfig>);
@NgModule({
	declarations: [
		PagesComponent,
		ActionComponent,
		ProfileComponent,
		ErrorPageComponent,
		InnerComponent,
		TrainingComponent,
		LoaderComponent,
		AddTrainingComponent,
		DetailTrainingComponent,
		
		AddTrainingCategoryComponent,
		AddOrganizationComponent,
		AddDistrictComponent,
		TrainedParticipantByCNICnameComponent,
		TrainingSummarybydistrictComponent,
		TrainingdetailreportbyfacilityComponent,
		AddDepartmentComponent,
		AddPlaceOfPostingComponent,
		
		AddScoreComponent,
		AddProgramComponent,
		AddPartnerComponent,
		AddTehseelComponent,
		HomeComponent,
		AddJobTitleComponent,
		AddProgramOtherModalComponent,
		SupportedByComponent,
		AddTrainingModuleComponent,
		ProductComponent,
		ProductModalComponent,
		AnalyticsComponent,
		LineComponent,
		RadarComponent,
		PieComponent,
		BarComponent,
		CorporatePartnerComponent,
		CorporatePartnerModalComponent
		
		
	],
	exports: [NgxMatSelectSearchModule],
	imports: [
		NgMultiSelectDropDownModule.forRoot(),
		NgxMaskModule.forRoot(),
		ChartModule,
		ChartsModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatCardModule,
		MatCheckboxModule,
		MatChipsModule,
		MatDatepickerModule,
		MatDialogModule,
		MatDividerModule,
		MatExpansionModule,
		MatFormFieldModule,
		MatGridListModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatNativeDateModule,
		MatPaginatorModule,
		NgxPaginationModule,
		MatProgressBarModule,
		MatProgressSpinnerModule,
		MatRadioModule,
		MatRippleModule,
		MatSelectModule,
		MatSidenavModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatSnackBarModule,
		MatSortModule,
		MatStepperModule,
		MatTableModule,
		MatTabsModule,
		MatToolbarModule,
		FormsModule,
		NgSelectModule,
		MatTooltipModule, MatButtonModule, MatCheckboxModule,
		ReactiveFormsModule,
		CommonModule,
		HttpClientModule,
		FormsModule,
		PagesRoutingModule,
		CoreModule,
		LayoutModule,
		PartialsModule,
		AngularEditorModule,
		NgxSpinnerModule,
		BsDatepickerModule.forRoot(),
		SweetAlert2Module.forRoot({
			buttonsStyling: false,
			customClass: 'modal-content',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn'
		})
	],
	providers: [{ provide: BsDatepickerConfig, useFactory: getDatepickerConfig }],
	entryComponents: [		AddTrainingCategoryComponent,
		AddOrganizationComponent,
		AddDistrictComponent,
		AddDepartmentComponent,
		AddPlaceOfPostingComponent,
		AddScoreComponent,
		AddProgramComponent,
		AddPartnerComponent,
		AddTehseelComponent,
		AddJobTitleComponent,
		AddProgramOtherModalComponent,
		SupportedByComponent,
		AddTrainingModuleComponent,
		ProductModalComponent
		
	]
})
export class PagesModule {
}
