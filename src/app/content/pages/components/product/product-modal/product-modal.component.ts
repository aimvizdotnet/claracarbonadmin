import { Component, EventEmitter, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ClaraCarbonService } from '../../../../../services/claraCarbon/clara-carbon.service';

@Component({
  selector: 'm-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss']
})
export class ProductModalComponent implements OnInit {

  productForm: FormGroup;
  @Input() data :any;
  public event: EventEmitter<any> = new EventEmitter();
  product : any = {};
  isChecked : boolean = true;
  productLength : any;
  User: any =  JSON.parse(localStorage.getItem('currentUser'));
  loading: boolean = false;
  message = ""
  @ViewChild('f') formValues;
  submited=false;
  constructor( 
    private _formBuilder: FormBuilder,
    // public dialogRef :MatDialogRef<ProductModalComponent>,
    private dialog: MatDialog,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private route: ActivatedRoute,
    private productService :  ClaraCarbonService
    // @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      // dialogRef.disableClose = true; 
     }

  ngOnInit() {
    
    // this.route.queryParamMap.subscribe(params => {
    //   this.data = params.get("id")

    // })
    this.countries= this.productService.getCountryList();
    if(this.data != null){
      
    this.product = this.data;
    if(!this.product.orderByCount)
    this.product.orderByCount=0;
    this.isChecked = this.data.isActive
    }else{
      this.isChecked = true;
      this.product.isActive = true;
    }

     this.productLength = Object.keys(this.product)
  
    
    this.productForm = this._formBuilder.group({
      name: ['', [Validators.required]],
      matchingName :[''],
      offsetCost:[''],
      estimateCarbonFootprint:[''],
      country:[''],
      changedBy:[''],
      description:[''],
      isActive:[''],
      orderByCount:['']
  });
//   if(this.data){
//     this.GetProductById()
//  }
  }
  GetProductById(){
    console.log("product")
   }
 
   getAllProducts(){ 
    console.log("products")
   }
   countries;
   Save(f){
    

    if(this.product.id){
      
      
      this.productService.editProduct(this.product).subscribe(res=>{
        
        if(res){
          console.log(res);
          this.loading=false;
          // this.message=res.message;
          this.popup("success" ,  "Record has been updated");
          this.message = "Record has been updated"

          this.activeModal.close;
          this.event.emit({ data: this.product });
        }
        else
        {
          this.loading=false;
          // this.message=res.message
          this.popup("error")
          
         }
        
      },
        err=>{
          this.loading=false;
          this.message="Request timed out please try again later"
          this.popup("warning","product",true)
    
      })
     }
    else{
        this.productService.createProduct(this.product).subscribe(res=>{
          
        if(res){
        console.log(res);
        this.loading=false;
        // this.message=res.message;
        this.popup("success" , 'Record has been added');
        this.message = 'Record has been added'
        this.activeModal.close;
        this.event.emit({ data: this.product });
      }
      else
      {
        this.loading=false;
        // this.message=res.message
        this.popup("error")
        
       }
      
    },
      err=>{
        this.loading=false;
        this.message="Request timed out please try again later"
        this.popup("warning","product",true)
  
    })}
   }

   onChange(event){
     
     this.isChecked = event.checked;




   }
   

   popup(status: any,title:any="Product",warning:boolean=false){

    Swal.fire({
      title: title,
      text: this.message,
      type: 'success',
    	showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ok!',
        

      
    }).then((result) => {
      if (result.value) {
        if (warning ==false) {
          this.activeModal.close();
        }
      }
    })


    // Swal.fire({
    //   title: 'Do you want to save the changes?',
    //    showDenyButton: true,
    //    showCancelButton: true,
    //    confirmButtonText: 'Save',
    //   denyButtonText: `Don't save`,
    // }).then((result) => {
    //   /* Read more about isConfirmed, isDenied below */
    //   if (result.isConfirmed) {
    //     Swal.fire('Saved!', '', 'success')
    //   } else if (result.isDenied) {
    //     Swal.fire('Changes are not saved', '', 'info')
    //   }
    // })

   }
}
