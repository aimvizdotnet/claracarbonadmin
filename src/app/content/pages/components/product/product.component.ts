import { Component, OnInit, ViewChild , AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource , PageEvent, Sort} from '@angular/material';
import { ProductModalComponent } from './product-modal/product-modal.component';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ClaraCarbonService } from '../../../../services/ClaraCarbon/clara-carbon.service';
import { C } from '@angular/core/src/render3';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

import {LiveAnnouncer} from '@angular/cdk/a11y';

@Component({
  selector: 'm-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  pageEvent;
  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  Products : any = [];
  isChecked : boolean = true
  isLoading = true;
  terms="";
	dataType:string="";
	showAllOperations = true;
	canAdd = true;
	canEdit = true;
	canDelete = true;
	statusList;
	from = '';
	to = '';
	searchItem = '';
	filterItem = 'Open';
	sortOrder = '';
	sortField = '';
	currentPage = 1;
	itemsPerPage = 10;
	pageStart = 1;
	currentListCount = 0;
  displayedColumns: string[] = ['name','description','offsetCost','estimateCarbonFootprint' , 'Actions'];
	filter: any = { type: '', dateRange: { StartDate: '', EndDate: '' } }
	today: Date = new Date();
  filterName:string='';
  // in constructor
  // private dialog: MatDialog,public dialogRef :MatDialogRef<ProductModalComponent>

  dataSource: MatTableDataSource<ProductData>;
  @ViewChild('paginator') paginator: MatPaginator;

  
  
  @ViewChild(MatSort) sort: MatSort;
 

  constructor(private modalService: NgbModal , 
    private productService : ClaraCarbonService ,
    private router : Router,
    private _liveAnnouncer: LiveAnnouncer
    ) { }


  ngOnInit() {
    
    this.isLoading = true;
    this.getAllproducts();
  }
  ngAfterViewInit (){

  }
  
  getServerData(pagination?: PageEvent) {
       this.getAllproducts(pagination.pageSize, pagination.pageIndex,this.filterName);
  }
    length=10;
 getAllproducts(pageSize = this.paginator.pageSize, pageIndex = this.paginator.pageIndex,name=''){
  this.isLoading = true;
   this.productService.getProducts(pageSize, (pageIndex+1),this.filterName).subscribe((res : any) => {
      if(res != null){
        console.log(res);
      
        this.isLoading = false;
        this.Products = res.data;
        this.paginator.length = res.count;
        this.dataSource = new MatTableDataSource(this.Products);
      }
     

   })

 }

 applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.getAllproducts(this.paginator.pageSize, this.paginator.pageIndex,this.filterName);
  // this.dataSource.filter = filterValue.trim().toLowerCase();

  // if (this.dataSource.paginator) {
    
  //   this.dataSource.paginator.firstPage();
  // }
}

announceSortChange(sortState: Sort) {
this.Products=this.Products.sort((a: any, b: any) => {
  if (a[sortState.active] < a[sortState.active]) {
      return 1;
  }
  else {
    return -1;
} 
});
this.dataSource = new MatTableDataSource(this.Products);
this.dataSource.paginator = this.paginator;
}


  deleteProduct(product){
    
    Swal.fire({
      title: "Delete",
      text: "Do you want to delete this record?",
      // icon: "error",
      showCancelButton: true,
      confirmButtonText: 'Ok',
      confirmButtonColor: '#2196f3'
    }).then((result) => {
      
      if (result.value) {
        this.productService .deleteProduct(product).subscribe(((res:any)=>{
          if(res.isSucess){
          this.popup("success" ,'Record has been deleted');
        }
        this.getAllproducts();
        }))
        
      }
    })
  }

 OpenDialog() {
    
    let ngbModalOptions: NgbModalOptions = {
      size:"lg",
      centered:true,
      windowClass:"modalclass",
      backdrop : 'static',
      keyboard : false
  }
  	const openModal = this.modalService.open(ProductModalComponent, ngbModalOptions);
    console.log(openModal)
    openModal.componentInstance.event.subscribe(data => {
      // do some logic
      console.log(data);
        openModal.close();
        this.getAllproducts();
      })
  }

  editDialog(product:any){

    // this.router.navigate(['editProduct'], { queryParams: { id : id } });
  	const openModal = this.modalService.open(ProductModalComponent,
			{
				size:"lg",
				centered:true,
				windowClass:"modalclass",

		});
    
    openModal.componentInstance.data = product;
    openModal.componentInstance.event.subscribe(data => {
      // do some logic
      console.log(data);
        openModal.close();
        this.getAllproducts();
      })
    
    // event.subscribe(data => {
    //   // do some logic
    //   console.log(data);
    //     openModal.close();
    //   })
  }
  popup(status,title:any="products",message : any = 'Delete',deleted:boolean = false){
    Swal.fire({
      title: title,
      text: message,
      type: status,
      showCancelButton: false,
      confirmButtonText: 'Ok',
      confirmButtonColor: '#2196f3'
    }).then((result) => {
      if (result.value) {
        if(deleted == false)
        {
        // this.dialogRef.close();
        this.getAllproducts();
        }
        else
        {
          this.getAllproducts();
        }
      }
    })
  }

    // Swal.fire({
    //   title: 'Do you want to save the changes?',
    //    showDenyButton: true,
    //    showCancelButton: true,
    //    confirmButtonText: 'Save',
    //   denyButtonText: `Don't save`,
    // }).then((result) => {
    //   /* Read more about isConfirmed, isDenied below */
    //   if (result.isConfirmed) {
    //     Swal.fire('Saved!', '', 'success')
    //   } else if (result.isDenied) {
    //     Swal.fire('Changes are not saved', '', 'info')
    //   }
    // })

   }



export interface ProductData {
  Id : any,
  name: string;
  matchingName: string;
  description: string;
  offsetCost: string;
  changedBy: string;
  country: string;
  isActive: boolean;
}