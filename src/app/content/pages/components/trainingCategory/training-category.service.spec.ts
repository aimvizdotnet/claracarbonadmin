import { TestBed } from '@angular/core/testing';

import { TrainingCategoryService } from './training-category.service';

describe('TrainingCategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrainingCategoryService = TestBed.get(TrainingCategoryService);
    expect(service).toBeTruthy();
  });
});
