import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { DepartmentService } from '../../department/department.service';
import { TrainingCategoryService } from '../training-category.service';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
  selector: 'm-add-training-category',
  templateUrl: './add-training-category.component.html',
  styleUrls: ['./add-training-category.component.scss']
})
export class AddTrainingCategoryComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() departmentId;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private departmentService: DepartmentService,
    private trainingCategoryService: TrainingCategoryService,public activeModal: NgbActiveModal
  ) { }
  TrainingCategoryModel: any = {
    TrainingCategoryId: '',
    departmentId: '',
    trainingCategoryName: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  Departments: any;
  ngOnInit() {
    this.getAllDepartments();
    this.TrainingCategoryModel.departmentId = this.departmentId;
  }
  getAllDepartments() {
    this.departmentService.getDepartments(this.obj).subscribe(data => {
      this.Departments = data;
      console.log(data);
    })
  }
  Save(form) {
    
    if (form.valid) {
      //Add work
      this.trainingCategoryService.saveTrainingCategory(form.value).subscribe(data => {
        if(data.status){
          console.log(data.id);

          this.event.emit({ trainingCategoryId: data.id });
          //swal.fire(data.message, '', 'success');
          this.formValues.resetForm({
            departmentId: ''
          });
        }
        else{
          swal.fire(data.message, '', 'error');
        }
      })
    }
  }
}
