import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrainingCategoryComponent } from './add-training-category.component';

describe('AddTrainingCategoryComponent', () => {
  let component: AddTrainingCategoryComponent;
  let fixture: ComponentFixture<AddTrainingCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTrainingCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrainingCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
