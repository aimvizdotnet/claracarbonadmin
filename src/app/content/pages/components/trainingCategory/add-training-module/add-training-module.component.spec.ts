import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrainingModuleComponent } from './add-training-module.component';

describe('AddTrainingModuleComponent', () => {
  let component: AddTrainingModuleComponent;
  let fixture: ComponentFixture<AddTrainingModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTrainingModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrainingModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
