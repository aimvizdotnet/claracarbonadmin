import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { TrainingCategoryService } from '../training-category.service';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
@Component({
  selector: 'm-add-training-module',
  templateUrl: './add-training-module.component.html',
  styleUrls: ['./add-training-module.component.scss']
})
export class AddTrainingModuleComponent implements OnInit {
  TrainingCategories: any;
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() trainingCategoryId;
  public event: EventEmitter<any> = new EventEmitter();

  constructor( private trainingCategoryService: TrainingCategoryService,public activeModal: NgbActiveModal) { }
  TrainingCategoryModuleModel: any = {
    trainingCategoryId: '',
    trainingCategoryModuleId: '',
    trainingCategoryModuleName: '',
    
  }
  ngOnInit() {
    this.getAllTrainingCategories()
    this.TrainingCategoryModuleModel.trainingCategoryId = this.trainingCategoryId;
  }
  getAllTrainingCategories() {
		this.trainingCategoryService.getTrainingCategories().subscribe(data => {
      
			this.TrainingCategories = data;
			//console.log(data);
		})
  }
  Save(form) {
    
    if (form.valid) {
      //Add work
      this.trainingCategoryService.saveTrainingCategoryModule(form.value).subscribe(data => {
        if(data.status){
          console.log(data.id);

          this.event.emit({ trainingCategoryModuleId: data.id });
          //swal.fire(data.message, '', 'success');
          this.formValues.resetForm({
            trainingCategoryId: ''
          });
        }
        else{
          swal.fire(data.message, '', 'error');
        }
      })
    }
  }
}
