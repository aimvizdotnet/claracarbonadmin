import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TrainingCategoryService { 
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getTrainingCategories(): any{
    return this._http.get(this.url + 'TrainingCategory/Get')
  }
  getTrainingModules(id): any{
    
    return this._http.get(this.url + 'TrainingCategory/GetModules?TrainingCategoryId='+id)
  }
  
  saveTrainingCategory(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'TrainingCategory/CreateTrainingCategory', obj);
    }
    
  }
  saveTrainingCategoryModule(obj: any): any {
    
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'TrainingCategory/CreateTrainingCategoryModule', obj);
    }
    
  }
  
}
