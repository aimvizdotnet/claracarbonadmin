import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { ClaraCarbonService } from '../../../../../services/claraCarbon/clara-carbon.service';

@Component({
  selector: 'bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {
  data : any = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = ['label a','label b'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: this.data, label: 'Series A' },
    { data: this.data, label: 'Series B' }
  ];

  constructor(private Productservice : ClaraCarbonService) { }

  ngOnInit() {  
    this.products();
    
  }

  products(){
  
    this.Productservice.getOffsets().subscribe((res : any) => {
       
      //  if(res.isSuccess){
        if(res != null){
          console.log("charts",res);
          //  let gg = res.obj;
          //  this.data = gg.map(x => x.balanceAmount)
          //  console.log(this.data)
          //  this.dataInit();
        }
        }
          
       
          //console.log(this.Products);
    //  }
    )}



  dataInit(){
  console.log("ye wala")
    this.barChartData.forEach(x => x.data = this.data)
  }
     
    }


