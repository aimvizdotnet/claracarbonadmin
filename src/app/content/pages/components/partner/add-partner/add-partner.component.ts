import { Component, OnInit, ViewChild, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PartnerService } from '../partner.service';

@Component({
  selector: 'm-add-partner',
  templateUrl: './add-partner.component.html',
  styleUrls: ['./add-partner.component.scss']
})
export class AddPartnerComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() Partners;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(public activeModal: NgbActiveModal, public partnerService: PartnerService) { }

  PartnerModel: any = {
    partnerId: '',
    partnerName: ''
  }

  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  alreayExist = false;
  isLoading = false;



  ngOnInit() {
  }
  Save(form) {
    
    if (form.valid) {
      this.isLoading = true;
      if (!this.Partners.find(el => el.partnerName.toLowerCase() === this.PartnerModel.PartnerName.toLowerCase())) {
        this.partnerService.savePartner(form.value).subscribe(data => {
          console.log(data.id);

          this.isLoading = false;
          this.event.emit({ partnerId: data.id });
          this.formValues.resetForm();
        })
      } else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    }
    else {
      this.isLoading = false;
    }
  }
}
