import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getPartners(organizationId) {
    return this._http.get(this.url + 'Partner/Get', {
      params: {
        organizationId
      }
    })
  }
  savePartner(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.createdBy = currentUser.userId;
      return this._http.post(this.url + 'Partner/SavePartner', obj);
    }
  }
}
