import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JobTitleService } from '../job-title.service';
import { QualificationService } from '../../qualification/qualification.service';

@Component({
  selector: 'm-add-job-title',
  templateUrl: './add-job-title.component.html',
  styleUrls: ['./add-job-title.component.scss']
})
export class AddJobTitleComponent implements OnInit {
  isLoading = false;
  formValid = true;
  alreayExist = false;
  jobTitle: any = {};
  @Input() jobTitles: any = {};
  public event: EventEmitter<any> = new EventEmitter();
  constructor(

    public activeModal: NgbActiveModal,
    public jobTitleService: JobTitleService,
    public qualificationService: QualificationService

  ) { }

  ngOnInit() {
  }
  Save(form) {
    
    if (form.valid) {
      this.isLoading = true;
      this.formValid = true;
      if (!this.jobTitles.find(el => el.jobTitleName.toLowerCase() === this.jobTitle.jobTitleName.toLowerCase())) {
        this.jobTitleService.SaveJobTitle(this.jobTitle)
          .subscribe(res => {
            this.isLoading = false;
            this.event.emit({ jobTitle: res.id });
          })
      } else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    } else {
      this.isLoading = false;
      this.formValid = false;
    }
  }
}
