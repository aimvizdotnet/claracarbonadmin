import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JobTitleService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }
  

  public SaveJobTitle(obj): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.createdBy = currentUser.userId;
      return this._http.post(this.url + 'Participant/SaveJobTitle', obj);
    }
  }

  public SaveSupportedBy(obj): any {
    
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.createdBy = currentUser.userId;
      return this._http.post(this.url + 'Training/InsertSupportBy', obj);
    }
  }
}
