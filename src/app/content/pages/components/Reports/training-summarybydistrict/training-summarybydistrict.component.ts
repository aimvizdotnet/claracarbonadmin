import { Component, OnInit } from '@angular/core';
import { ReportService } from '../report.service';

@Component({
  selector: 'm-training-summarybydistrict',
  templateUrl: './training-summarybydistrict.component.html',
  styleUrls: ['./training-summarybydistrict.component.scss']
})
export class TrainingSummarybydistrictComponent implements OnInit {

  constructor(private reportService: ReportService) { }

  model = [];
  ddlDistrict: any = {
    district: ''
  };
  paging: any = {
    PageSize: 10,
    CurrentPage: 1,
  };
  District;

  ngOnInit() {
    this.GetAllDistrict();

  }
  GetAllDistrict() {
    this.reportService.GetAllDistrict().subscribe(res => {
      this.District = res;
    })
  }
  pageChanged(event) {
    this.paging.CurrentPage = event;
  }
  GetByDistrict() {
    this.reportService.GetByDistrict(this.ddlDistrict).subscribe(res => {
      this.model = res;
    })
  }
}
