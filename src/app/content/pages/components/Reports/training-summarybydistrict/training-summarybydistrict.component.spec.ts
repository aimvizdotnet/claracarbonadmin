import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingSummarybydistrictComponent } from './training-summarybydistrict.component';

describe('TrainingSummarybydistrictComponent', () => {
  let component: TrainingSummarybydistrictComponent;
  let fixture: ComponentFixture<TrainingSummarybydistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingSummarybydistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingSummarybydistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
