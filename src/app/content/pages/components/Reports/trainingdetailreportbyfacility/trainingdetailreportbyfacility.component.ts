import { Component, OnInit } from '@angular/core';
import { ReportService } from '../report.service';

@Component({
  selector: 'm-trainingdetailreportbyfacility',
  templateUrl: './trainingdetailreportbyfacility.component.html',
  styleUrls: ['./trainingdetailreportbyfacility.component.scss']
})
export class TrainingdetailreportbyfacilityComponent implements OnInit {

  constructor(private reportService: ReportService) { }
  paging: any = {
    PageSize: 10,
    CurrentPage: 1,
  };
  model = [];
  ddlFacility: any = {
    facility: ''
  };
  Facility;
  pageChanged(event) {
    this.paging.CurrentPage = event;
  }
  ngOnInit() {
    this.GetAllFacility();
  }
  GetByFacility() {
    this.reportService.GetByFacility(this.ddlFacility).subscribe(res => {
      this.Facility = res;
    });
  }
  GetAllFacility() {
    this.reportService.GetAllFacility().subscribe(res => {
      this.Facility = res;
    });
  }

}
