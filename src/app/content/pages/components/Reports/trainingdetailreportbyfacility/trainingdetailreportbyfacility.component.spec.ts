import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingdetailreportbyfacilityComponent } from './trainingdetailreportbyfacility.component';

describe('TrainingdetailreportbyfacilityComponent', () => {
  let component: TrainingdetailreportbyfacilityComponent;
  let fixture: ComponentFixture<TrainingdetailreportbyfacilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingdetailreportbyfacilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingdetailreportbyfacilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
