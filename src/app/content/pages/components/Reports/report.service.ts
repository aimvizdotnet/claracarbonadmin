import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  GetNameByCNIC(cnic): any {
    return this._http.post(this.url + 'Trainer/GetNameByCNIC', cnic);
  }

  GetCNIC(cnic) {
    let obj = { cnic: cnic };
    var listofCnic = this._http.post(this.url + 'Trainer/SearchCNIC', obj)
      .pipe(
        debounceTime(500),  // WAIT FOR 500 MILISECONDS ATER EACH KEY STROKE.
        map(
          (data: any) => {
            return (
              data.cnic.length != 0 ? data.cnic as any[] : [{ "cnic": "No Record Found" } as any]
            );
          }
        ));
    return listofCnic;
  }
  GetParticipantData(model): any {
    return this._http.post(this.url + 'Report/GetParticipantList', model);
  }
  GetByDistrict(ddlDistrict): any {
    return this._http.post(this.url + 'Report/GetByDistrict', ddlDistrict);
  }
  GetByFacility(ddlDistrict): any {
    return this._http.get(this.url + 'Report/GetByFacility',ddlDistrict);
  }
  GetAllDistrict(): any {
    return this._http.get(this.url + 'Report/GetAllDistrict');
  }
  GetAllFacility(): any {
    return this._http.get(this.url + 'Report/GetAllFacility');
  }
  
}
