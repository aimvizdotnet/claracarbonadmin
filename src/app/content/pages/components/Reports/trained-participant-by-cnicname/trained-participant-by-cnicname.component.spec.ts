import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainedParticipantByCNICnameComponent } from './trained-participant-by-cnicname.component';

describe('TrainedParticipantByCNICnameComponent', () => {
  let component: TrainedParticipantByCNICnameComponent;
  let fixture: ComponentFixture<TrainedParticipantByCNICnameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainedParticipantByCNICnameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainedParticipantByCNICnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
