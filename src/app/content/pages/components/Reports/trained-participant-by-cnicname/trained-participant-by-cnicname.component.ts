import { Component, OnInit } from '@angular/core';
import { ReportService } from '../report.service';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { switchMap, debounceTime } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material';

@Component({
  selector: 'm-trained-participant-by-cnicname',
  templateUrl: './trained-participant-by-cnicname.component.html',
  styleUrls: ['./trained-participant-by-cnicname.component.scss']
})
export class TrainedParticipantByCNICnameComponent implements OnInit {
isLoading;
  constructor(private reportService: ReportService) { }
  model = {
    name: '',
    cnic: ''
  }
  paging: any = {
    PageSize: 10,
    CurrentPage: 1,
  };
  nicList;
  filteredCnic: Observable<any>;
  reportForm: FormGroup;
  searchTerm: FormControl = new FormControl();
  CNICObj = <any>[];
  reportData;
  ngOnInit() {
    this.searchTerm.valueChanges.subscribe(
      term => {
        if (term != '') {
          this.reportService.GetCNIC(term).subscribe(
            data => {
              this.CNICObj = data as any[];
              //console.log(data[0].BookName);
            })
        }
      })
  }
  GetParticipantData() {
    this.reportService.GetParticipantData(this.model).subscribe(res => {
      this.reportData = res;
      console.log(this.reportData);
    })
  }
  pageChanged(event) {
    this.paging.CurrentPage = event;
  }

  GetNameByCnic(cnic){
    this.model.cnic = cnic;
    this.reportService.GetNameByCNIC(this.model).subscribe(res => {
      if (res.isSuccess) {
        this.model.name = res.data;
        console.log(this.model);
      }
      console.log(res);
    })
  }

}
