import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddScoreComponent } from '../../scores/add-score/add-score.component';
import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'm-detail-training',
  templateUrl: './detail-training.component.html',
  styleUrls: ['./detail-training.component.scss']
})
export class DetailTrainingComponent implements OnInit {
	currentUser = JSON.parse(localStorage.getItem("currentUser"));
  canAddP = true;
  canEditT = true;
  canAddTR = true;
  canDeleteTR = true;
  showAllOperations = true;
  previousUrl = '';
  selectedTab =1;
  searchItem = '';
  sortOrder = '';
  sortField = '';
  currentPage = 1;
  nominationCurrentPage = 1;
  nominationItemsPerPage = 10;
	totalNomination = 0;
  itemsPerPage = 10;
	totalItems = 10;
	totalParticipants = 0;
  pageStart = 1;
  currentListCount = 0;
  nominations: any;

	constructor(private route: ActivatedRoute,
	 private modalService: NgbModal,
    private router: Router,
  ) { }
  TrainingModel: any = {
    Id: '',
    status: '',
    departmentId: '',
    departmentName: '',
    participantTarget: '',
    instituteId: '',
    instituteName: '',
    participantAchieved: '',
    trainingCategoryId: '',
    trainingCategoryName: '',
    organizationId: '',
    organizationName: '',
    courseId: '',
    courseName: '',
    districtId: '',
    districtName: '',
    cadreId: '',
    cadreName: '',
    venueId: '',
    venueName: '',
    startDate: '',
    comment: '',
    endDate: '',

  }
  isLoading = false;
  Participants: any = {};
  Trainers: any = {};
  Cadres: any;
  Courses: any;
  Districts: any;
  Institutes: any;
  Organizations: any;
  TrainingCategories: any;
  Venues: any;
  Departments: any;

  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  trainingId;
  item = '';


  getParticipantsByTrainingId(item, sortField, sortOrder, id, itemsPerPage, currentPage) {
    //item = this.item;
    //sortField = this.sortField;
    //sortOrder = this.sortOrder;
    //itemsPerPage = this.itemsPerPage;
    //currentPage = this.currentPage;

    this.isLoading = false;
    // this.participantService.getParticipantsByTrainingId(item, sortField, sortOrder, id, itemsPerPage, currentPage).subscribe(res => {
		// this.totalParticipants = res.Count;
    //   this.Participants = res.data;
		// this.isLoading = false;
		// this.changeDetectorRef.detectChanges();
    // })
  }
  getTrainerByTrainingId(item, sortField, sortOrder, id, itemsPerPage, currentPage) {
    //item = this.item;
    //sortField = this.sortField;
    //sortOrder = this.sortOrder;
    //itemsPerPage = this.itemsPerPage;
    //currentPage = this.currentPage;

    this.isLoading = true;
    
  }
  
  pageChanged(event) {
    //console.log(event);
    this.currentPage = event;

    //this.getAllTrainings(this.searchItem, this.sortField, this.sortOrder);
  }
  pageChanged2($event){
    this.nominationCurrentPage = $event;
  }
  ngOnInit() {
    //this.allRights();
    this.canAddParticipant();
    this.canEditTraining();
    
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("id")) {
        this.trainingId = paramMap.get("id");
        //service method call
        this.getParticipantsByTrainingId(this.item, this.sortField, this.sortOrder, this.trainingId, this.itemsPerPage, this.currentPage);
        this.getTrainerByTrainingId(this.item, this.sortField, this.sortOrder, this.trainingId, this.itemsPerPage, this.currentPage);
        this.GetNominationByTrainingId(this.item, this.sortField, this.sortOrder, this.trainingId, this.itemsPerPage, this.currentPage);
      }
    })
  }
  sortData(e) {
    //console.log(e);
    this.getParticipantsByTrainingId(this.item, e.active, e.direction, this.trainingId, this.itemsPerPage, this.currentPage);
  }

  updateTraining(status, id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, ' + status + ' training!'
    }).then((result) => {
      if (result.value) {
        let obj = { Id: id, status }
        //this.router.navigate(['trainings']);
        //alert('Gaya..');
      }
    })


  }

  
	navigateToParticipant(trainingCategoryId, courseId, districtId, venueId, trainingId) {
		
    let obj = { trainingCategoryId: trainingCategoryId.toString(),
      districtId: districtId.toString(), venueId: venueId.toString(), trainingId: trainingId.toString() };
      console.log(obj);
    //this.participantService.sendData(obj);
    this.router.navigate(['participants/addNew']);
  }
	navigateToNomination(trainingCategoryId, courseId, districtId, venueId, trainingId) {
		
    let obj = { trainingCategoryId: trainingCategoryId.toString(),
		districtId: districtId.toString(), venueId: venueId.toString(), trainingId: trainingId.toString() };
    console.log(obj);
    //this.participantService.sendData(obj);
    this.router.navigate(['nominations/addNewNomination']);
  }

  openScoreModal(obj) {

    const openModalForAddScore = this.modalService.open(AddScoreComponent);
    openModalForAddScore.componentInstance.TrainingModel = this.TrainingModel;
    openModalForAddScore.componentInstance.ParticipantModel = obj;

    openModalForAddScore.componentInstance.event.subscribe(data => {
      //   this.venueService.getVenues(this.TrainingModel.districtId).subscribe(res => {
      //     this.Venues = res;
      //     this.TrainingModel.venueId = data.venueId;
      //     //console.log(data);
      //   })
      console.log(data);
      //console.log(this.Participants);

      // this.participantService.getParticipantsByTrainingId(this.item, this.sortField, this.sortOrder, data.obj.trainingId, this.itemsPerPage, this.currentPage).subscribe(res => {
        
      //   this.totalItems = res.Count;
      //   this.Participants = res.data;
      //   this.isLoading = false;

      //   console.log(this.Participants);
      //   openModalForAddScore.close();
      // })

    })

  }

  GetNominationByTrainingId(item, sortField, sortOrder, trainingId, itemsPerPage, currentPage) {
    
    this.isLoading = true;
   
  }

  DisAssociateTrainer(trainerTrainingId) {
    swal.fire({
      title: 'Are you sure?',
      text: "You want to remove trainer to this training!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.isLoading = true;
       
      }
    })
  }

  NavigateToTrainer(trainingCategoryId, courseId, districtId, venueId,trainingId) {
    let obj = { 
      trainingCategoryId: trainingCategoryId.toString(),
      districtId: districtId.toString(), 
      venueId: venueId.toString(),
      trainingId:trainingId.toString()
    };
    this.router.navigate(['trainer/addNew']);
  }
  allRights(){
	}
  canAddParticipant(){
		//this.canAddP = this.participantService.canAddParticipant();
  }
  canEditTraining(){
	}
  
}


