import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Input, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ClaraCarbonService } from '../../../../../services/ClaraCarbon/clara-carbon.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

interface Interval {
	value: number;
	viewValue: string;
  }

@Component({
	selector: 'm-add-training',
	templateUrl: './add-training.component.html',
	styleUrls: ['./add-training.component.scss']
})

export class AddTrainingComponent implements OnInit {

	public event: EventEmitter<any> = new EventEmitter();

	currentUser = JSON.parse(localStorage.getItem('currentUser'));
	dropdownSettings: IDropdownSettings = {};
	countryDropdownSettings: IDropdownSettings = {};
	
	isLoading:boolean=false;
	dropdownList: any = [];
	dropdownListCopy: any = [];
	selectedItems: any = [];
	value: string;
	viewValue: string;
	selectedCountries = [];
	countryList: any = [];
	statusActive: boolean;
	statusInActive: boolean;
	filter = {
		statusActive: false,
		statusInActive: false,
		country: [],
		users: []
	};

	@Input() public Id;



	
	myControl = new FormControl();
	@ViewChild('f') formValues;
	DepartmentsCtrl: FormControl;
	today: Date = new Date();
	Notification: any = {};
	filterUser: any = {};
	startDate: Date = new Date();
	numbers;
	constructor(private router: Router, public activeModal: NgbActiveModal, public ClaraCarbonService: ClaraCarbonService) {

		this.numbers = Array(23).fill(1).map((x,i)=>i+1);
	}
	dropdownList1 = [];

	ngOnInit() {

		
		this.getUsers();
		this.dropDownSettings();
		this.filter.country = this.ClaraCarbonService.getCountryList();

		if(this.Id != null){
			this.GetNotificationById();
		}
		
		
	}
	intervals: Interval[] = [
		{value: 0, viewValue: 'Daily'},
		{value: 1, viewValue: 'Weekly'},
		{value: 2, viewValue: 'Monthly'},
	  ];

	GetNotificationById(){

		this.isLoading=true;
		this.ClaraCarbonService.getNotificationById(this.Id).subscribe((res:any)=>{

		
			
			if(res.isSuccess){

				
				this.Notification.Title=res.obj.title;
				this.Notification.Interval=res.obj.interval;
				this.Notification.Description=res.obj.description;
				this.Notification.IsActive=res.obj.isActive;
				
				var date=new Date(res.obj.dateTimeSent);
				//var date2=moment(date).format("YYYY-DD-MM hh:mm").replace(" ","T");
				// var date2=moment(date).format("YYYY-MM-DDTkk:mm");
				this.Notification.NotificationDate=date
				this.Notification.Hour = date.getHours().toString();
				this.selectedItems=res.selectedUser;
				this.Notification.IsReschedule=res.obj.isReschedule

	this.isLoading=false;

			}
			this.isLoading=false;

		},err=>{
			this.isLoading=false;
		})


	}

	dropDownSettings() {

		this.countryDropdownSettings = {
			singleSelection: false,
			idField: 'code',
			textField: 'name',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};

		this.dropdownSettings = {
			singleSelection: false,
			idField: 'id',
			textField: 'email',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};
	}

	getUsers(): any {
		this.isLoading=true;
		this.ClaraCarbonService.getUsers().subscribe((res: any) => {
			this.isLoading=false;
			if (res.isSuccess) {

				this.dropdownListCopy = res.obj
				this.filter.users = res.obj
				this.filter.users.map(x=>{x.isDisabled = (x.expoToken?false:true)})
		

			}
		},err=>{

			this.isLoading=false;
		})
	}

	statusFilter(event, value) {
		
		var checked = event.target.checked;
		//if active is true clicked and in active is false
		if (checked && value == 1 && !this.filter.statusInActive) {

			this.filter.users = this.dropdownListCopy.filter(x => x.status == 1);

		}
		//if active is and in active is already true
		else if (!checked && value == 1 && this.filter.statusInActive) {

			this.filter.users = this.dropdownListCopy.filter(x => x.status != 1);

		}

		//if in active is false and active is true
		else if (!checked && value != 1 && this.filter.statusActive) {


			this.filter.users = this.dropdownListCopy.filter(x => x.status == 1);

		}
		else if (checked && value != 1 && !this.filter.statusActive) {

			this.filter.users = this.dropdownListCopy.filter(x => x.status != 1);

		}
		else {

			this.filter.users = this.dropdownListCopy;
		}
	}


	onCountrySelect(item: any) {
		this.filterCountries();
	}
	onSelectAllCountries(items: any) {

		if (this.filter.statusInActive == this.filter.statusActive) {

			this.filter.users = this.dropdownListCopy;
		}
		else {
			if (this.filter.statusActive) {

				this.filter.users = this.dropdownListCopy.filter(x => x.status == 1);
			}
			else {

				this.filter.users = this.dropdownListCopy.filter(x => x.status != 1);

			}

		}
	}
	onCountryDeSelect(event) {
		this.filterCountries();
	}

	filterCountries() {
		if (this.selectedCountries.length <= 0) {
			this.filter.users = this.dropdownListCopy;
		}
		else {
			this.filter.users = [];
			this.dropdownListCopy.forEach(element => {
				
				var data = this.selectedCountries.find(x => x.name == element.country)
				if (data != null) {

					this.filter.users.push(element);
				}
			});
			if (this.filter.statusActive != this.filter.statusInActive) {
				if (this.filter.statusActive) {
					this.filter.users.filter(x => x.status == 1)
				}
				if (this.filter.statusInActive) {
					this.filter.users.filter(x => x.status != 1)
				}

			}

		}
	}



	onItemSelect(item: any) {
		console.log(item);
	}
	onSelectAll(items: any) {
		console.log(items);
	}





	Save(f: any) {
		console.log(f.value.NotificationDate)
		if (!f.invalid) {
			swal.fire({
				title: 'Are you sure?',
				text: "This will send notification to selected users!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then((result) => {
				if (result.value) {
					f.value.NotificationDate.setHours(f.value.Hour);
					f.value.NotificationDate.setMinutes(0);
					f.value.NotificationDate.setSeconds(0);
					this.submit(f);

				}
			})
		}
	}

	submit(f: any) {
		
		if(this.Id != null){
			this.Updatenotification(f);
		}
		else{
			this.addnotification(f);
		}

		


	}

	addnotification(f:any){

	
		this.isLoading=true;
		var data = f.value;
		this.ClaraCarbonService.saveNotification(data).subscribe(res => {

			this.isLoading=false;
			this.event.emit({ data:data});
		},err=>{
			this.isLoading=false;
			console.log(err);
		})
	}


	Updatenotification(f:any){

		
		this.isLoading=true
		var data = f.value;
		data.Id=this.Id;
		this.ClaraCarbonService.UpdateNotification(data).subscribe((res:any) => {

			
			this.isLoading=false;
			this.event.emit({ data:res});
		},err=>{
			this.isLoading=false;
			console.log(err);
		})
	}


	getStartDate(startDate) {
		var date1, date2;
		date2 = startDate;

		if (date1 < date2) {
		}
		else {
			var res = Math.abs(date1 - date2) / 1000;

		}
	}







	backToList() {
		swal.fire({
			title: 'Are you sure?',
			text: "This will send notification to selected users",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then((result) => {
			if (result.value) {

				this.router.navigate(['notification']);

			}
		})
	}



}
