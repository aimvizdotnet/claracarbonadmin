import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProgramOtherModalComponent } from './add-program-other-modal.component';

describe('AddProgramOtherModalComponent', () => {
  let component: AddProgramOtherModalComponent;
  let fixture: ComponentFixture<AddProgramOtherModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProgramOtherModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProgramOtherModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
