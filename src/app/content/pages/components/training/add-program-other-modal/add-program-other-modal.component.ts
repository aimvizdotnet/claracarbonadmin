import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { DepartmentService } from '../../department/department.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'm-add-program-other-modal',
  templateUrl: './add-program-other-modal.component.html',
  styleUrls: ['./add-program-other-modal.component.scss']
})
export class AddProgramOtherModalComponent implements OnInit {
  @Input() departmentProgramId;
  public event: EventEmitter<any> = new EventEmitter();
  ProgramOtherModel: any = {
    DepartmentProgramsId: '',
    Name: ''
  }
  alreayExist = false;
  isLoading = false;

  constructor(private departmentService: DepartmentService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  Save(form) {
    
    if (form.valid) {
      this.isLoading = true;
      //if (!this.Programs.find(el => el.departmentProgramName.toLowerCase() === this.ProgramModel.departmentProgramName.toLowerCase())) {
        //Add work
        this.ProgramOtherModel.DepartmentProgramsId = this.departmentProgramId;
        this.departmentService.SaveProgramOther(this.ProgramOtherModel).subscribe(data => {
          this.event.emit({ departmentProgramId: data.id });
          //this.formValues.resetForm();
        })
      // }
      // else {
      //   this.isLoading = false;
      //   this.alreayExist = true;
      // }
    }
    else{
      this.isLoading = false;
    }
  }

}
