import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClaraCarbonService } from '../../../../../services/ClaraCarbon/clara-carbon.service';
import { AddTrainingComponent } from '../add-training/add-training.component';
import swal from 'sweetalert2';
import { MatPaginator, MatSort, MatTableDataSource, PageEvent, Sort } from '@angular/material';

@Component({
	selector: 'm-training',
	templateUrl: './training.component.html',
	styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit, OnDestroy {
	paging: any;
	itemsPerPage: number;
	currentPage: number;
	totalItems: any;
	search:any;
	filterName: string;
	duplicate:any;
	displayedColumns: string[] = ['title','description','createdAt', 'dateTimeSent','userEmail', 'Actions'];
	pageEvent;
	dataSource: MatTableDataSource<any>;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
  
	ngOnDestroy() {
		
	}

	isLoading:boolean=false;
	notificationList:any = [];

	
	constructor(
		public claraCarbonService:ClaraCarbonService,
		private modalService: NgbModal,
	) {

	}
	
	




	ngOnInit() {

		 this.itemsPerPage=15;
		 this.currentPage=1;
		this.getNotification();
	
	}

	getServerData(pagination?: PageEvent) {
		this.getNotification(pagination.pageSize, pagination.pageIndex,this.filterName);
   }
	 length=10;



	getNotification(limit: number = 10, page: number = 0, filter: string = ''){

		this.isLoading=true;
		this.claraCarbonService.getNotification(limit, (page + 1), filter).subscribe((res:any)=>{
	    this.isLoading=false;
			if(res != null){
				console.log(res);
				console.log("dada",res)
			
				this.notificationList=res.data;
				this.duplicate=res.data;
				this.totalItems=res.data.length;
				// let data = res.filter(x => x.isActive == true);
				this.notificationList = this.notificationList.sort((a: any, b: any) => {
				  if (a.title.toUpperCase() < b.title.toUpperCase()) {
					  return 1;
				  } 
				  if (a.title.toUpperCase() > b.title.toUpperCase()) {
					return -1;
				} 
			  });
			  	this.paginator.length = res.count;
				this.dataSource = new MatTableDataSource(this.notificationList);
				// this.dataSource.paginator = this.paginator;
			
				//console.log(this.Products);
			  }


			// this.isLoading=false;
			// if(res.isSuccess){
			// 	console.log(res.obj)
			// 	this.notificationList=res.obj;
			// 	this.duplicate=res.obj;
			// 	this.totalItems=res.obj.length;
			// }

		
		},
		err=>{
			this.isLoading=false;
		})
	
	}


	announceSortChange(sortState: Sort) {
		
		// This example uses English messages. If your application supports
		// multiple language, you would internationalize these strings.
		// Furthermore, you can customize the message to add additional
		// details about the values being sorted.
		// if (sortState.direction) {
		//   this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
		  
		// } else {
		//   this._liveAnnouncer.announce('Sorting cleared');
		// }
		
		
	  
		// this.dataSource = new MatTableDataSource(this.notificationList);
	    // this.dataSource.data.sort((a: any, b: any) => {
	    //   if (a.name.toUpperCase() < b.name.toUpperCase()) {
	    //       return 1;
	    //   } 
	    //   if (a.name.toUpperCase() > b.name.toUpperCase()) {
	    //     return -1;
		// }})
		  


		this.notificationList=this.notificationList.sort((a: any, b: any) => {
			if (a[sortState.active] < a[sortState.active]) {
				return 1;
			}
			else {
			  return -1;
		  } 
		  });
		  this.dataSource = new MatTableDataSource(this.notificationList);
		  this.dataSource.paginator = this.paginator;
		  
	};
	    
	
	pageChanged(event) {
		this.currentPage = event;
	  }


	OpenTraining(id=null){

		
		const openModal = this.modalService.open(AddTrainingComponent,
			{
				size:"lg",
				centered:true,
				windowClass:"modalclass",
				backdrop:false

		});
		
		openModal.componentInstance.Id=id;
		openModal.componentInstance.event.subscribe(data => {
			
			openModal.close();

		
				this.getNotification();
		
		})


	}

	deleteNotification(id){


		swal.fire({
			title: 'Are you sure You want to delete this notification?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((result) => {
			if (result.value) {

				this.isLoading=true;
				this.claraCarbonService.deleteNotification(id).subscribe((res:any)=>{

					this.isLoading=false;
					this.getNotification();



				},err=>{

					this.isLoading=false;
				})

		

			}
		})




	

	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	  
		if (this.dataSource.paginator) {
		  
		  this.dataSource.paginator.firstPage();
		}
	  }
	

	sortData(sortby: any) {
		

		//sorting by first name
		if (sortby.active == "Title") {
		  if (sortby.direction == "asc") {
			this.notificationList  = this.notificationList.sort((a: any, b: any) => 0 - (a.title > b.title ? 1 : -1));
		  }
		  if(sortby.direction =="desc") {
	
			this.notificationList = this.notificationList.sort((a: any, b: any) => 0 - (a.title > b.title ? -1 : 1));
			//this.notificationList = this.notificationList .sort((a: any, b: any) => b.title-a.title);
	
		  }
		}
		if (sortby.active == "Description") {
			if (sortby.direction == "asc") {
			  //Order Descending
			  //this.notificationList = this.notificationList .sort((a: any, b: any) => a.title-b.title);
			  this.notificationList  = this.notificationList.sort((a: any, b: any) => 0 - (a.description > b.description ? 1 : -1));
	  
			  
			}
			
			if(sortby.direction =="desc") {
	  
			  this.notificationList = this.notificationList.sort((a: any, b: any) => 0 - (a.description > b.description ? -1 : 1));
			  //this.notificationList = this.notificationList .sort((a: any, b: any) => b.title-a.title);
	  
			}
		  }

		  if (sortby.active == "DateCreated") {
			if (sortby.direction == "asc") {
			 
			//this.notificationList  = this.notificationList.sort((a: any, b: any) => 0 - (a.description > b.description ? 1 : -1));
			  this.notificationList  =this.notificationList.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime());

	  
			  
			}
			
			if(sortby.direction =="desc") {
	  
				
				this.notificationList  =this.notificationList.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());

			  //this.notificationList = this.notificationList .sort((a: any, b: any) => b.title-a.title);
	  
			}
		  }

		  if (sortby.active == "DateNotification") {
			if (sortby.direction == "asc") {
			 
			//this.notificationList  = this.notificationList.sort((a: any, b: any) => 0 - (a.description > b.description ? 1 : -1));
			  this.notificationList  =this.notificationList.sort((a, b) => new Date(b.dateTimeSent).getTime() - new Date(a.dateTimeSent).getTime());

	  
			  
			}
			
			if(sortby.direction =="desc") {
	  
				
				this.notificationList  =this.notificationList.sort((a, b) => new Date(a.dateTimeSent).getTime() - new Date(b.dateTimeSent).getTime());

			  //this.notificationList = this.notificationList .sort((a: any, b: any) => b.title-a.title);
	  
			}
		  }




	}

	searching(e){
		
		if(this.search.length==0){

			this.notificationList=this.duplicate;
		}
		else{
			this.notificationList=this.duplicate.filter(x=>{ return x.title.toLocaleLowerCase().includes(this.search) || x.description.toLocaleLowerCase().includes(this.search)  || x.userEmail.toString().toLocaleLowerCase().includes(this.search)})
			
		}


	}
}
	


