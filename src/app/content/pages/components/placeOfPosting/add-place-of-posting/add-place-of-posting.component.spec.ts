import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlaceOfPostingComponent } from './add-place-of-posting.component';

describe('AddPlaceOfPostingComponent', () => {
  let component: AddPlaceOfPostingComponent;
  let fixture: ComponentFixture<AddPlaceOfPostingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlaceOfPostingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlaceOfPostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
