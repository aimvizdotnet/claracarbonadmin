import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DepartmentService } from '../../department/department.service';
import { DistrictService } from '../../district/district.service';
import { PlaceOfPostingService } from '../place-of-posting.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TehseelService } from '../../tehseel/tehseel.service';

@Component({
  selector: 'm-add-place-of-posting',
  templateUrl: './add-place-of-posting.component.html',
  styleUrls: ['./add-place-of-posting.component.scss']
})
export class AddPlaceOfPostingComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() departmentId;
  @Input() districtId;
  @Input() tehseelId;
  @Input() facilityTypeId;

  public event: EventEmitter<any> = new EventEmitter();
  constructor(private departmentService: DepartmentService,
    private districtService: DistrictService,
    private placeOfPostingService: PlaceOfPostingService,
    public activeModal: NgbActiveModal,
    private tehseelService: TehseelService
    ) { }
    PlaceOfPostingModel: any = {
      PlaceOfPostingId: '',
      departmentId: '',
      districtId: '',
      facilityTypeId: '',
      placeOfPostingName: '',
      tehseelId: ''
    }
    obj: any = {
      CurrentPage: 0,
      itemsPerPage: 0,
      sortDirection: '',
      sortField: ''
    }
    Departments: any;
    Districts: any;
    FacilityTypes: any;
    Tehseels: any;
  ngOnInit() {
    this.getAllDepartments();
    this.getAllFacilityTypes();
    this.getAllDistricts();
    //alert(this.departmentId);
    this.PlaceOfPostingModel.departmentId = this.departmentId;
    this.PlaceOfPostingModel.districtId = this.districtId;
    
    this.PlaceOfPostingModel.facilityTypeId = this.facilityTypeId; 
    
    this.getCurrentDept();

    this.tehseelService.getTehseels(this.PlaceOfPostingModel.districtId).subscribe(data => {
      this.Tehseels = data;
      //console.log(data);
      this.PlaceOfPostingModel.tehseelId = this.tehseelId;
    })
  }

  getCurrentDept() {
    
  }
  // getSelectedDepartmentId(deptId){
  //   //this.getAllDistricts(deptId);
  // }
  getAllDepartments() {
    this.departmentService.getDepartments(this.obj).subscribe(data => {
      this.Departments = data;
      //console.log(data);
    })
  }
  getAllDistricts() {
    this.districtService.getDistricts().subscribe(data => {
      this.Districts = data;
      //console.log(data);
    })
  }
	getAllFacilityTypes() {
		
		
  }
  getAllTehseels(districtId) {
    this.tehseelService.getTehseels(districtId).subscribe(data => {
      this.Tehseels = data;
      //console.log(data);
      //this.PlaceOfPostingModel.tehseelId = this.tehseelId;
    })
  }
  selectedDistrict(id) {
    this.getAllTehseels(id);
  }
  Save(form) {
    
    if (form.valid) {
      //Add work
      form.value.departmentId = this.PlaceOfPostingModel.departmentId;
      this.placeOfPostingService.savePlaceOfPosting(form.value).subscribe(data => {
		  console.log(data.id);
		  
		  let facilityType = this.FacilityTypes.find(x => x.facilityTypeId == this.PlaceOfPostingModel.facilityTypeId);
		  let facilityTypeName = '';
		  if (facilityType) {
			  facilityTypeName = facilityType.facilityTypeName;
		  }
		  else
			  facilityTypeName = '';

		  this.event.emit({
			  placeOfPostingId: data.id, departmentId: this.PlaceOfPostingModel.departmentId, districtId: this.PlaceOfPostingModel.districtId,
			  tehseelId: this.PlaceOfPostingModel.tehseelId, facilityTypeName: facilityTypeName
        });
        //swal.fire(data.message, '', 'success');
        this.formValues.resetForm({
          departmentId: '',
          districtId: '',
          //facilityTypeId: ''
          tehseelId: ''
        });
      })
    }
  }

  GetFacilityWithDepartmentId(id){
    
  }
}
