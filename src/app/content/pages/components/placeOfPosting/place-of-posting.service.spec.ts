import { TestBed } from '@angular/core/testing';

import { PlaceOfPostingService } from './place-of-posting.service';

describe('PlaceOfPostingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceOfPostingService = TestBed.get(PlaceOfPostingService);
    expect(service).toBeTruthy();
  });
});
