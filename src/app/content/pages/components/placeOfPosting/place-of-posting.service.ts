import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlaceOfPostingService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getPlaceOfPostings(obj: any): any {
  
    return this._http.get(this.url + 'PlaceOfPosting/Get')
	}

  savePlaceOfPosting(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'PlaceOfPosting/CreatePlaceOfPosting', obj);
    }
  }
  getPlaceOfPostingsByDeptIdDistrictIdFacilityTypeId(deptId, districtId, facilityTypeId) {
    return this._http.get(this.url + 'PlaceOfPosting/GetPlaceOfPostingsByDeptIdDistrictIdFacilityTypeId', {
      params: {
        deptId: deptId,
        districtId: districtId,
        facilityTypeId: facilityTypeId
      }
    })
  }
  getPlaceOfPostingsByDeptIdDistrictIdTehseelId(deptId, districtId, tehseelId) {
    
    return this._http.get(this.url + 'PlaceOfPosting/GetPlaceOfPostingsByDeptIdDistrictIdTehseelId', {
      params: {
        deptId: deptId != undefined ? deptId : 0,
        districtId: districtId != undefined ? districtId : 0,
        tehseelId: tehseelId != undefined ? tehseelId : 0
      }
    })
  }
  GetPlaceOfPostingsByDeptIdDistrictId(deptId, districtId) {
    return this._http.get(this.url + 'PlaceOfPosting/GetPlaceOfPostingsByDeptIdDistrictId', {
      params: {
        deptId: deptId,
        districtId: districtId
      }
    })
  }
}
