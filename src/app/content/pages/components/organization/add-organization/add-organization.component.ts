import { Component, OnInit, ViewChild, EventEmitter, Input } from '@angular/core';
import { OrganizationService } from '../organization.service';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'm-add-organization',
  templateUrl: './add-organization.component.html',
  styleUrls: ['./add-organization.component.scss']
})
export class AddOrganizationComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() Organizations;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private organizationService: OrganizationService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  OrganizationModel: any = {
    OrganizationId: '',
    //departmentId: '',
    //trainingCategoryId: '',
    organizationName: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  alreayExist = false;
  isLoading = false

  Save(form) {

    if (form.valid) {
      this.isLoading = true;
      if (!this.Organizations.find(el => el.organizationName.toLowerCase() === this.OrganizationModel.organizationName.toLowerCase())) {
        this.organizationService.saveOrganization(form.value).subscribe(data => {

          this.isLoading = false;
          this.event.emit({ organizationId: data.id });
          //swal.fire(data.message, '', 'success');
          this.formValues.resetForm();
        })
      } else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    } else {
      this.isLoading = false;
    }
  }
}
