import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getOrganizations(obj: any): any {
    return this._http.get(this.url + 'Organization/Get')
  }

  saveOrganization(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'Organization/CreateOrganization', obj);
    }
  }
}
