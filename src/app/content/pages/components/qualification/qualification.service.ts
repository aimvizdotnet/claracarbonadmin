import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QualificationService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getQualifications(obj: any): any {
    return this._http.get(this.url + 'Qualification/Get')
  }

  public SaveQualification(obj): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'Qualification/SaveQualification', obj);
    }
  }
}
