import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DepartmentService } from '../department.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'm-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss']
})
export class AddDepartmentComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private departmentService: DepartmentService,public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  DepartmentModel: any = {
    DepartmentId: '',
    //departmentId: '',
    //trainingCategoryId: '',
    departmentName: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  Save(form) {
    
    if (form.valid) {
      //Add work
      this.departmentService.saveDepartment(form.value).subscribe(data => {
        //console.log(data.id);

        this.event.emit({ departmentId: data.id });
        //swal.fire(data.message, '', 'success');
        this.formValues.resetForm();
      })
    }
  }
}
