import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getDepartments(obj: any): any {
    return this._http.get(this.url + 'Department/Get')
  }
  getRoles(obj: any): any {
    return this._http.get(this.url + 'Department/GetRoles')
  }
  getPrograms(obj: any): any {
    return this._http.get(this.url + 'Department/GetPrograms')
  }

  saveDepartment(obj: any): any {
    return this._http.post(this.url + 'Department/SaveDepartment', obj)
  }
  saveProgram(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.createdBy = currentUser.userId;
      return this._http.post(this.url + 'Department/SaveProgram', obj);
    }
  }

  SaveProgramOther(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.createdBy = currentUser.userId;
      return this._http.post(this.url + 'Department/SaveProgramOther', obj);
    }
  }
}
