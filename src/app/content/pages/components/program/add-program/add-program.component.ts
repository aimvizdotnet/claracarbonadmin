import { Component, OnInit, ViewChild, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DepartmentService } from '../../department/department.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'm-add-program',
  templateUrl: './add-program.component.html',
  styleUrls: ['./add-program.component.scss']
})
export class AddProgramComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() Programs;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private departmentService: DepartmentService, public activeModal: NgbActiveModal) { }
  ProgramModel: any = {
    departmentProgramId: '',
    departmentProgramName: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  alreayExist = false;
  isLoading = false;

  ngOnInit() {
  }
  Save(form) {
    
    if (form.valid) {
      this.isLoading = true;
      console.log(this.Programs);
      if (!this.Programs.find(el => el.departmentProgramName.toLowerCase() === this.ProgramModel.departmentProgramName.toLowerCase())) {
        //Add work
        this.departmentService.saveProgram(form.value).subscribe(data => {
          console.log(data.id);

          this.event.emit({ departmentProgramId: data.id });
          //swal.fire(data.message, '', 'success');
          this.formValues.resetForm();
        })
      }
      else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    }
    else{
      this.isLoading = false;
    }
  }
}
