import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportedByComponent } from './supported-by.component';

describe('SupportedByComponent', () => {
  let component: SupportedByComponent;
  let fixture: ComponentFixture<SupportedByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportedByComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportedByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
