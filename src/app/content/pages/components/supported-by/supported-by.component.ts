import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { QualificationService } from '../qualification/qualification.service';
import { JobTitleService } from '../jobTitle/job-title.service';

@Component({
  selector: 'm-supported-by',
  templateUrl: './supported-by.component.html',
  styleUrls: ['./supported-by.component.scss']
})
export class SupportedByComponent implements OnInit {
  isLoading = false;
  formValid = true;
  alreayExist = false;
  SupportedByList: any = {};
  @Input() supportedBy: any = {};
  public event: EventEmitter<any> = new EventEmitter();
  constructor(

    public activeModal: NgbActiveModal,
    public jobTitleService: JobTitleService,
    public qualificationService: QualificationService

  ) { }

  ngOnInit() {
    
  }
  Save(form) {
    
    if (form.valid) {
      this.isLoading = true;
      this.formValid = true;
      if (!this.SupportedByList.find(el => el.supportName.toLowerCase() === this.supportedBy.supportName.toLowerCase())) {
        this.jobTitleService.SaveSupportedBy(this.supportedBy)
          .subscribe(res => {
            this.isLoading = false;
            this.event.emit({ jobTitle: res.id });
          })
      } else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    } else {
      this.isLoading = false;
      this.formValid = false;
    }
  }
}