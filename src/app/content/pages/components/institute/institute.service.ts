import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InstituteService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getInstitutes(obj: any): any{
    return this._http.get(this.url + 'Institute/Get')
  }
}
