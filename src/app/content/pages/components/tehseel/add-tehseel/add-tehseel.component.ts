import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DistrictService } from '../../district/district.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TehseelService } from '../tehseel.service';

@Component({
  selector: 'm-add-tehseel',
  templateUrl: './add-tehseel.component.html',
  styleUrls: ['./add-tehseel.component.scss']
})
export class AddTehseelComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  @Input() districtId;
  @Input() Tehseels;
  isLoading = false;
  alreayExist = false;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(
    private districtService: DistrictService,
    public activeModal: NgbActiveModal,
    private tehseelService: TehseelService
  ) { }
  TehseelModel: any = {
    tehseelId: '',
    districtId: '',
    tehseelName: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  Districts: any;
  ngOnInit() {
    this.getAllDistricts();
    this.getAllTehseels(this.districtId);
    this.TehseelModel.districtId = this.districtId;
  }
  getAllDistricts() {
    this.districtService.getDistricts().subscribe(data => {
      this.Districts = data;
      //console.log(data);
    })
  }
  Save(form) {
   
    if (form.valid) {
      this.isLoading = true;
      if (!this.Tehseels.find(el => el.tehseelName.toLowerCase() === this.TehseelModel.tehseelName.toLowerCase())) {
        this.tehseelService.saveTehseel(form.value).subscribe(data => {
          this.isLoading = false;
			this.event.emit({ tehseelId: data.tehseelId, tehseelName: data.tehseelName });
          this.formValues.resetForm({
            districtId: ''
          });
        })
      } else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    } else {
      this.isLoading = false;
    }
  }
  getAllTehseels(districtId) {
    this.tehseelService.getTehseels(districtId).subscribe(data => {
      this.Tehseels = data;
      //console.log(data);
    })
  }
  onDistrictChange(districtId){
    this.getAllTehseels(districtId);
  }
}
