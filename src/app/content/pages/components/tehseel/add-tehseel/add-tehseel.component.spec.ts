import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTehseelComponent } from './add-tehseel.component';

describe('AddTehseelComponent', () => {
  let component: AddTehseelComponent;
  let fixture: ComponentFixture<AddTehseelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTehseelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTehseelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
