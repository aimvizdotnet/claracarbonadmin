import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TehseelService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  getTehseels(districtId) {
    return this._http.get(this.url + 'Venue/GetTehseels', {
      params: {
        districtId
      }
    })
  }
  saveTehseel(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'Venue/CreateTehseel', obj);
    }
  }
}
