import { Component, OnInit, ViewChild } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';
import { LineComponent } from '../charts/line/line.component'
import { RadarComponent } from '../charts/radar/radar.component';
import { PieComponent } from '../charts/pie/pie.component';
import { BarComponent } from '../charts/bar/bar.component';
import { getMatIconFailedToSanitizeLiteralError, MatCard, MatGridTile } from '@angular/material';

import { ClaraCarbonService } from '../../../../services/ClaraCarbon/clara-carbon.service';
import { Chart, ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import * as moment from 'moment';
import { ChartService } from '../../../../services/Chart/chart.service';
import { ExcelService } from '../../../../services/excel/excel.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import swal from 'sweetalert2';
@Component({
  selector: 'analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
  filter: { startDate: Date, endDate: Date, interval: string, products: any[] } = { startDate: (new Date), endDate: (new Date), interval: 'daily', products: [] };
  countryDropdownSettings: IDropdownSettings = {};
  isOffsetsLoaded: boolean = false;;
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
          ticks: {
            suggestedMax:5,
            min: 0,
            beginAtZero: true,
          }
      }]
  },
  };
  public SubscriptionChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
          ticks: {
            min: 0,
            suggestedMax:5,
            stepSize:1,
            beginAtZero: true,
          }
      }]
  },
  };
  public offsetLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  excelData: any;
  public totalOffsetsData: ChartDataSets[] = [];
  public totalOffsetSpentData: ChartDataSets[] = [];
  public totalSubscriptionData: ChartDataSets[] = [];
  constructor(private productService: ClaraCarbonService, private chartsService: ChartService, private excelService: ExcelService) { }
  products = [];
  @ViewChild('item2') divToScroll: HTMLUListElement;
  serchText = '';
  search = { size: 1000, index: 0, text: '' }
  searching: boolean = true;
  IslastData: boolean = false;
  minDate = new Date();
  maxDate = new Date();


  getUnique(a) {
    var unique = [];
    a.map(x => {
      if (unique.length == 0 || unique.indexOf(z => z.id == x.id) < 0) {
        unique.push(x)
      }
    }
    );
    return unique;
  }
  
  getProducts(search = '') {
    this.productService.getProducts(this.search.size, (this.search.index + 1), search).subscribe((res: any) => {
      if (res != null) {
        res.data.map(x=> x.full_name=x.name+' - ('+ (x.country?x.country:'N/A')+')')
        this.products = res.data;
          this.IslastData = true;
      }
    })
  }
  getDays(from,to){
    let a = moment(from);
    let b = moment(to);
    let diff = b.diff(a, 'days')
    return diff;
  }
  changeFromDate(e) {
    this.minDate = this.filter.startDate;
    this.maxDate =moment(this.filter.startDate).add(30, 'day').toDate();
  }
  changeToDate(e) {
    this.minDate = this.filter.startDate;
    this.maxDate =moment(this.filter.startDate).add(30, 'day').toDate();
  }

  ngAfterViewInit() {

  }
  ngOnInit() {
    this.countryDropdownSettings = {
      enableCheckAll: false,
      idField: 'id',
      textField: 'full_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 0,
      allowSearchFilter: true,
      noDataAvailablePlaceholderText: "",
    };
    let d = new Date();
    this.filter.endDate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
    this.filter.startDate = moment(this.filter.startDate).add(-7, 'day').toDate();
    this.minDate = this.filter.startDate;
    this.maxDate =moment(this.filter.startDate).add(30, 'day').toDate();
    this.getOffsetsChart();
    this.getProducts();
  }
  downloadExcel() {
    this.excelService.exportAsExcelFile(this.excelData, "OffsetsReport")
  }
  donloadGraph(id) {
    console.log('canvas nae => ', id)
    var canvasElement = document.getElementById(id) as HTMLCanvasElement;
    var MIME_TYPE = "image/png";
    var imgURL = canvasElement.toDataURL(MIME_TYPE);
    var dlLink = document.createElement('a');
    dlLink.download = 'OffsetsData';
    dlLink.href = imgURL;
    dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');
    document.body.appendChild(dlLink);
    dlLink.click();
    document.body.removeChild(dlLink);
  }
  getOffsetsChart() {
    this.isOffsetsLoaded = false;
    if (this.getDays(this.filter.startDate,this.filter.endDate) > 30) {
      //this.filter.startDate= new Date();
      this.isOffsetsLoaded = true;
      swal.fire({
				title: 'Alert',
				text: "You can not select date range greater than 30 days!",
				type: 'warning',
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
			})
    }
    else
    {
      this.filter.products.map(x=> x.name=x.full_name.split("-")[0].trim())
        this.chartsService.GetOffsetsStatsForAdmin(this.filter).subscribe(res => {
      //res.excelData.map(x=>x.createdDate=new Date(x.createdDate));
      this.excelData = res.excelData;
      this.offsetLabels = res.chartData.labels;
      this.totalOffsetsData = res.chartData.data.filter(x=>x.stack=="TotalOffset");
      this.totalOffsetSpentData = res.chartData.data.filter(x=>x.stack=="TotalSpent");
      this.totalSubscriptionData = res.chartData.data.filter(x=>x.stack=="Subscrition");
      this.isOffsetsLoaded = true;
    })
    }
  
  }
}
