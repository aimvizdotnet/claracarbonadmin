import { TestBed } from '@angular/core/testing';

import { SupportbyService } from './supportby.service';

describe('SupportbyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SupportbyService = TestBed.get(SupportbyService);
    expect(service).toBeTruthy();
  });
});
