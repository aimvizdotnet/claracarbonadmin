import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SupportbyService {

  url:string = environment.server;
  constructor(private _http:HttpClient) { }

  getAllSupport(){
    return this._http.get(this.url+'Training/GetAllSupport')
  }
}
