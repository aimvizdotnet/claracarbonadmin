import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {
  url: string = environment.server;
	constructor(private _http: HttpClient) { }
	
  getDistricts(): any {
    
    let user= JSON.parse(localStorage.getItem("currentUser"));
    let showAll = (!user.showUserwiseData).toString();
	  return this._http.get(this.url + 'District/Get', {params: {
      userId:user.userId,
      showAll
    }})
  }
  saveDistrict(obj: any): any {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      obj.userId = currentUser.userId;
      return this._http.post(this.url + 'District/CreateDistrict', obj);
    }
  }
  getDistrictsByDeptId(deptId) {
    
    let user= JSON.parse(localStorage.getItem("currentUser"));
    let showAll = (!user.showUserwiseData).toString();
    return this._http.get(this.url + 'District/GetDestrictsByDepartmentId', {
      params: {
        deptId,
        showAll
      }
    })
  }
}
