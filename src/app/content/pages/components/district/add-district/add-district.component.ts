import { Component, OnInit, ViewChild, EventEmitter, Input } from '@angular/core';
import { DistrictService } from '../district.service';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'm-add-district',
  templateUrl: './add-district.component.html',
  styleUrls: ['./add-district.component.scss']
})
export class AddDistrictComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;
  alreayExist = false;
  isLoading = false;
	@Input() districts;
	@Input() public ProvinceId;
  public event: EventEmitter<any> = new EventEmitter();
  constructor(private districtService: DistrictService, public activeModal: NgbActiveModal) { }

	ngOnInit() {
		this.DistrictModel.ProvinceId = '5';// for kpk
  }
	DistrictModel: any = {
		
    DistrictId: '',
    //departmentId: '',
    //trainingCategoryId: '',
    districtName: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  Save(form) {
    
    if (form.valid) {
      this.isLoading = true;
		if (!this.districts.find(el => el.districtName.toLowerCase() === this.DistrictModel.districtName.toLowerCase())) {
			this.districtService.saveDistrict(this.DistrictModel).subscribe(data => {
          //console.log(data.id);

          this.event.emit({ districtId: data.id });
          this.formValues.resetForm();
        })
      } else {
        this.isLoading = false;
        this.alreayExist = true;
      }
    } else {
      this.isLoading = false;
    }
  }
}
