import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';


@Component({
  selector: 'm-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  @ViewChild('content') myDiv: ElementRef;
  constructor(private modalService: NgbModal) { }
  // ,public activeModal: NgbActiveModal
  ngOnInit() {
    // this.openSm(this.myDiv);
  }
  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }
  // public dismiss(): void {
  //   this.activeModal.close("close");
  // }
}
