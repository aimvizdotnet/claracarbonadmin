import { TestBed } from '@angular/core/testing';

import { TypeoftraineeService } from './typeoftrainee.service';

describe('TypeoftraineeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeoftraineeService = TestBed.get(TypeoftraineeService);
    expect(service).toBeTruthy();
  });
});
