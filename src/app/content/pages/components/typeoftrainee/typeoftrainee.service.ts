import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypeoftraineeService {

  url:string = environment.server;
  constructor(private _http:HttpClient) { }

  getTypeOfTrainees(){
    return this._http.get(this.url + 'Training/GetTypeOfTrainees');
	}

	AddTypeOfTrainee(name) {
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		return this._http.post(this.url + 'Participant/AddTypeOfTrainee', { TypeOfTraineeName: name, UserId: currentUser.userId});
	}
}
