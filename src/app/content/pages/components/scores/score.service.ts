import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  url: string = environment.server;
  constructor(private _http: HttpClient) { }

  AddScoresAgainstPIdAndTId(obj: any): any {
    return this._http.post(this.url + 'Training/AddScoresAgainstPIdAndTId', obj)
  }
}
