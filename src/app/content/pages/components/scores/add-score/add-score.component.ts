import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ScoreService } from '../score.service';
import swal from 'sweetalert2';

@Component({
  selector: 'm-add-score',
  templateUrl: './add-score.component.html',
  styleUrls: ['./add-score.component.scss']
})
export class AddScoreComponent implements OnInit {
  myControl = new FormControl();
  @ViewChild('f') formValues;

  // @Input() participantId;
  // @Input() trainingId;

  @Input() TrainingModel;
  @Input() ParticipantModel;
  
  public event: EventEmitter<any> = new EventEmitter();
  constructor(public activeModal: NgbActiveModal, private scoreService: ScoreService) { }
  ScoreModel: any = {
    participantId: '',
    trainingId: '',
    pretestScore: '',
    posttestScore: ''
  }
  obj: any = {
    CurrentPage: 0,
    itemsPerPage: 0,
    sortDirection: '',
    sortField: ''
  }
  ngOnInit() {
  }
  Save(form) {
    
    //console.log(this.TrainingModel)
    //console.log(this.ParticipantModel)
    if (form.valid) {
      //Add work
      //console.log("TrainingId =>", this.TrainingModel.id);
      //console.log("ParticipantId =>", this.ParticipantModel.participantId);
      //console.log(form.value);
      let obj = {pretestScore: form.value.pretestScore, posttestScore: form.value.posttestScore, trainingId:this.TrainingModel.id,
        participantId: this.ParticipantModel.participantId
      }
      this.scoreService.AddScoresAgainstPIdAndTId(obj).subscribe(data => {
        swal.fire(data.message, "", "success");
        this.formValues.resetForm();

        this.event.emit({ obj });
      })
      // this.courseService.saveCourse(form.value).subscribe(data => {
      //   console.log(data.id);

      //   this.event.emit({ courseId: data.id });
        
      //   this.formValues.resetForm({
      //     departmentId: '',
      //     trainingCategoryId: ''
      //   });
      // })
    }
  }
}
