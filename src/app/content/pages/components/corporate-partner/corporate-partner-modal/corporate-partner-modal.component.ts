import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, Input, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ClaraCarbonService } from '../../../../../services/ClaraCarbon/clara-carbon.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { CorporatePartnerServiceService } from '../../../../../services/CorporatePartnerService/corporate-partner-service.service';

@Component({
  selector: 'm-corporate-partner-modal',
  templateUrl: './corporate-partner-modal.component.html',
  styleUrls: ['./corporate-partner-modal.component.scss']
})
export class CorporatePartnerModalComponent implements OnInit {
	@ViewChild('Image', { read: ElementRef })myInputVariable: ElementRef;


	public event: EventEmitter<any> = new EventEmitter();

	currentUser = JSON.parse(localStorage.getItem('currentUser'));
	dropdownSettings: IDropdownSettings = {};
	countryDropdownSettings: IDropdownSettings = {};
	isLoading:boolean=false;
	dropdownList: any = [];
	dropdownListCopy: any = [];
	selectedItems: any = [];
	selectedCountries = [];
	countryList: any = [];
	statusActive: boolean;
	statusInActive: boolean;
	filearrya:File[];
  images:any;
	filter = {
		statusActive: false,
		statusInActive: false,
		country: [],
		users: []
	};

	@Input() public Id;



	
	myControl = new FormControl();
	@ViewChild('f') formValues;
	DepartmentsCtrl: FormControl;
	today: Date = new Date();
	CorporatePartner: any = {};
	filterUser: any = {};
	startDate: Date = new Date();

	constructor(private router: Router, 
    public activeModal: NgbActiveModal, 
    public CorporatePartnerservice:CorporatePartnerServiceService,
    public ClaraCarbonService: ClaraCarbonService) {
	}

	ngOnInit() {
		
		//this.getUsers();
		//this.dropDownSettings();
		this.filter.country = this.ClaraCarbonService.getCountryList();

		if(this.Id != null){
			this.GetCorporatePartnerById();
		}
	}

	GetCorporatePartnerById(){

    
		this.isLoading=true;
		this.CorporatePartnerservice.getCorporatePartnerById(this.Id).subscribe((res:any)=>{

		
			
			if(res.isSuccess){

				
				this.CorporatePartner.CorporatePartnerName=res.obj.corporatePartnerName;
				this.CorporatePartner.Description=res.obj.description;
				this.CorporatePartner.Url=res.obj.url;
				this.images=res.obj.image
				this.isLoading=false;

			}
			this.isLoading=false;

		},err=>{
			this.isLoading=false;
		})


	}

  //#region dropDownSettings



	dropDownSettings() {

		this.countryDropdownSettings = {
			singleSelection: false,
			idField: 'code',
			textField: 'name',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};

		this.dropdownSettings = {
			singleSelection: false,
			idField: 'id',
			textField: 'email',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: true
		};
	}
  //#endregion


  //#region  getuser
  getUsers() {
		this.isLoading=true;
		this.ClaraCarbonService.getUsers().subscribe((res: any) => {
			this.isLoading=false;
			if (res.isSuccess) {

				this.dropdownListCopy = res.obj
				this.filter.users = res.obj
				console.log(res.obj);
			}
		},err=>{

			this.isLoading=false;
		})
	}
//#endregion
	

statusFilter(event, value) {
		
		var checked = event.target.checked;
		//if active is true clicked and in active is false
		if (checked && value == 1 && !this.filter.statusInActive) {

			this.filter.users = this.dropdownListCopy.filter(x => x.status == 1);

		}
		//if active is and in active is already true
		else if (!checked && value == 1 && this.filter.statusInActive) {

			this.filter.users = this.dropdownListCopy.filter(x => x.status != 1);

		}

		//if in active is false and active is true
		else if (!checked && value != 1 && this.filter.statusActive) {


			this.filter.users = this.dropdownListCopy.filter(x => x.status == 1);

		}
		else if (checked && value != 1 && !this.filter.statusActive) {

			this.filter.users = this.dropdownListCopy.filter(x => x.status != 1);

		}
		else {

			this.filter.users = this.dropdownListCopy;
		}
	}


	// onCountrySelect(item: any) {
	// 	this.filterCountries();
	// }
	// onSelectAllCountries(items: any) {

	// 	if (this.filter.statusInActive == this.filter.statusActive) {

	// 		this.filter.users = this.dropdownListCopy;
	// 	}
	// 	else {
	// 		if (this.filter.statusActive) {

	// 			this.filter.users = this.dropdownListCopy.filter(x => x.status == 1);
	// 		}
	// 		else {

	// 			this.filter.users = this.dropdownListCopy.filter(x => x.status != 1);

	// 		}

	// 	}
	// }
	// onCountryDeSelect(event) {
	// 	this.filterCountries();
	// }

	filterCountries() {
		if (this.selectedCountries.length <= 0) {
			this.filter.users = this.dropdownListCopy;
		}
		else {
			this.filter.users = [];
			this.dropdownListCopy.forEach(element => {
				
				var data = this.selectedCountries.find(x => x.name == element.country)
				if (data != null) {

					this.filter.users.push(element);
				}
			});
			if (this.filter.statusActive != this.filter.statusInActive) {
				if (this.filter.statusActive) {
					this.filter.users.filter(x => x.status == 1)
				}
				if (this.filter.statusInActive) {
					this.filter.users.filter(x => x.status != 1)
				}

			}

		}
	}



	onItemSelect(item: any) {
		console.log(item);
	}
	onSelectAll(items: any) {
		console.log(items);
	}





	Save(f: any) {

		if (!f.invalid) {
			swal.fire({
				title: 'Are you sure?',
				text: "This will send notification to selected users!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then((result) => {
				if (result.value) {

					this.submit(f);

				}
			})
		}
	}

	submit(f: any) {
		if(this.Id != null){
			this.Updatenotification(f);
		}
		else{
			this.addnotification(f);
		}
	}

  onFileSelected(e:any){
    
	var size=e.target.files[0].size;
	//size in kb
	size=size/1024
	if(size > 30)
	{
		setTimeout(() => {
			this.myInputVariable.nativeElement.value = "";
			
		});
		//this.myInputVariable
		//e.target.files=this.filearrya;
		//this.CorporatePartner.Image="";
		this.images=null;
		swal.fire({
			title: 'Size should be less then 30 kb',
			type: 'warning',
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK'
		})
		
		return false;
	}
	




	this.fileToBase64(e.target.files[0]).then(res=>{
		this.images=res;
		
  
	   });
  
  }

	async addnotification(f:any){

		this.isLoading=true;
		var data = f.value;
		data.Image=this.images;
		this.CorporatePartnerservice.CreateCorporatePartner(data).subscribe(res => {

       
			this.isLoading=false;
			this.event.emit({ data:data});
		  },err=>{
			this.isLoading=false;
			console.log(err);
		  })
	
	
 
 
		
	}


	Updatenotification(f:any){

		this.isLoading=true
		var data = f.value;
		data.Image=this.images;
		data.Id=this.Id;
		this.CorporatePartnerservice.UpdateCorporatePartner(data).subscribe((res:any) => {

			
			this.isLoading=false;
			this.event.emit({ data:res});
		},err=>{
			this.isLoading=false;
			console.log(err);
		})
	}


	getStartDate(startDate) {
		var date1, date2;
		date2 = startDate;

		if (date1 < date2) {
		}
		else {
			var res = Math.abs(date1 - date2) / 1000;

		}
	}

  fileToBase64 = (file: File): Promise<string> => {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.toString());
      reader.onerror = error => reject(error);
    })
  }







	backToList() {
		swal.fire({
			title: 'Are you sure?',
			text: "This will send notification to selected users",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then((result) => {
			if (result.value) {

				this.router.navigate(['notification']);

			}
		})
	}



}

