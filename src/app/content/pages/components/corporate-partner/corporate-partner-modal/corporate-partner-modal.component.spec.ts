import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporatePartnerModalComponent } from './corporate-partner-modal.component';

describe('CorporatePartnerModalComponent', () => {
  let component: CorporatePartnerModalComponent;
  let fixture: ComponentFixture<CorporatePartnerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporatePartnerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporatePartnerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
