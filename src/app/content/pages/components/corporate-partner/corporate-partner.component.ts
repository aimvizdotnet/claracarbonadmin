import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClaraCarbonService } from '../../../../services/ClaraCarbon/clara-carbon.service';
import swal from 'sweetalert2';
import { MatPaginator, MatSort, MatTableDataSource, PageEvent, Sort } from '@angular/material';
import { CorporatePartnerModalComponent } from './corporate-partner-modal/corporate-partner-modal.component';
import { CorporatePartnerServiceService } from '../../../../services/CorporatePartnerService/corporate-partner-service.service';

@Component({
	selector: 'm-corporate-partner',
	templateUrl: './corporate-partner.component.html',
	styleUrls: ['./corporate-partner.component.scss']
})
export class CorporatePartnerComponent implements OnInit {
	paging: any;
	Corporate: any = [];
	itemsPerPage: number;
	currentPage: number;
	totalItems: any;
	search: any;
	duplicate: any;
	displayedColumns: string[] = ['corporatePartnerName', 'description', 'createdAt', 'url', 'image', 'clickCount', 'Actions'];
	pageEvent;
	dataSource: MatTableDataSource<any>;
	@ViewChild('paginator') paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	filterName: string;
	length: any;
	ngOnDestroy() {

	}

	isLoading: boolean = false;
	CorporatePartnerList: any = [];


	constructor(
		public CorporatePartnerservice: CorporatePartnerServiceService,
		public claraCarbonService: ClaraCarbonService,
		private modalService: NgbModal,
	) {

	}






	ngOnInit() {

		this.itemsPerPage = 15;
		this.currentPage = 1;
		this.getCorporatePartner();

	}
	getServerData(pagination?: PageEvent) {
		this.getCorporatePartner(pagination.pageSize, pagination.pageIndex, this.filterName)
	}
	
	
	getCorporatePartner(limit: number = 10, page: number = 0, filter: string = '') {

		this.isLoading = true;
		this.CorporatePartnerservice.getCorporatePartners(limit, (page + 1), filter).subscribe((res: any) => {
			this.isLoading = false;
			if (res != null) {
				
				// console.log(res);
				// console.log("length",res.data.length)
				//this.length =res.data.length;
				
				// this.CorporatePartnerList=res.data;
				// this.duplicate=res.data;
				// this.totalItems=res.data.length;
			
			
			
				// this.dataSource.paginator = this.paginator;
				this.Corporate = res.data;
				// this.length = (res.totalPages * this.paginator.pageSize);
				this.dataSource = new MatTableDataSource(this.Corporate);
				// this.dataSource.paginator = this.paginator;
				this.paginator.length = res.count;
				
				//console.log(this.Products);    (res.totalPages * this.paginator.pageSize);
			}




		},
			err => {
				this.isLoading = false;
			})

	}


	announceSortChange(sortState: Sort) {
		




		this.CorporatePartnerList = this.CorporatePartnerList.sort((a: any, b: any) => {
			if (a[sortState.active] < a[sortState.active]) {
				return 1;
			}
			else {
				return -1;
			}
		});
		this.dataSource = new MatTableDataSource(this.CorporatePartnerList);
		//this.dataSource.paginator = this.paginator;

	};


	pageChanged(event) {
		this.currentPage = event;
	}


	OpenTraining(id = null) {

		
		const openModal = this.modalService.open(CorporatePartnerModalComponent,
			{
				size: "lg",
				centered: true,
				windowClass: "modalclass",
				backdrop: false

			});

		openModal.componentInstance.Id = id;
		openModal.componentInstance.event.subscribe(data => {

			openModal.close();


			this.getCorporatePartner();

		})


	}

	deleteCorporatePartner(id) {


		swal.fire({
			title: 'Are you sure You want to delete this CorporatePartner?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then((result) => {
			if (result.value) {

				this.isLoading = true;
				this.CorporatePartnerservice.deleteCorporatePartner(id).subscribe((res: any) => {

					this.isLoading = false;
					this.getCorporatePartner();



				}, err => {

					this.isLoading = false;
				})



			}
		})






	}

	applyFilter(event: Event) {
		// const filterValue = (event.target as HTMLInputElement).value;
		// this.dataSource.filter = filterValue.trim().toLowerCase();

		// if (this.dataSource.paginator) {

		//   this.dataSource.paginator.firstPage();
		// }
		this.getCorporatePartner(this.paginator.pageSize, this.paginator.pageIndex, this.filterName)
	}


	sortData(sortby: any) {
		

		//sorting by first name
		if (sortby.active == "Title") {
			if (sortby.direction == "asc") {
				this.CorporatePartnerList = this.CorporatePartnerList.sort((a: any, b: any) => 0 - (a.title > b.title ? 1 : -1));
			}
			if (sortby.direction == "desc") {

				this.CorporatePartnerList = this.CorporatePartnerList.sort((a: any, b: any) => 0 - (a.title > b.title ? -1 : 1));
				//this.CorporatePartnerList = this.CorporatePartnerList .sort((a: any, b: any) => b.title-a.title);

			}
		}
		if (sortby.active == "Description") {
			if (sortby.direction == "asc") {
				//Order Descending
				//this.CorporatePartnerList = this.CorporatePartnerList .sort((a: any, b: any) => a.title-b.title);
				this.CorporatePartnerList = this.CorporatePartnerList.sort((a: any, b: any) => 0 - (a.description > b.description ? 1 : -1));


			}

			if (sortby.direction == "desc") {

				this.CorporatePartnerList = this.CorporatePartnerList.sort((a: any, b: any) => 0 - (a.description > b.description ? -1 : 1));
				//this.CorporatePartnerList = this.CorporatePartnerList .sort((a: any, b: any) => b.title-a.title);

			}
		}

		if (sortby.active == "DateCreated") {
			if (sortby.direction == "asc") {

				//this.CorporatePartnerList  = this.CorporatePartnerList.sort((a: any, b: any) => 0 - (a.description > b.description ? 1 : -1));
				this.CorporatePartnerList = this.CorporatePartnerList.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime());



			}

			if (sortby.direction == "desc") {


				this.CorporatePartnerList = this.CorporatePartnerList.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());

				//this.CorporatePartnerList = this.CorporatePartnerList .sort((a: any, b: any) => b.title-a.title);

			}
		}

		if (sortby.active == "DateCorporatePartner") {
			if (sortby.direction == "asc") {

				//this.CorporatePartnerList  = this.CorporatePartnerList.sort((a: any, b: any) => 0 - (a.description > b.description ? 1 : -1));
				this.CorporatePartnerList = this.CorporatePartnerList.sort((a, b) => new Date(b.dateTimeSent).getTime() - new Date(a.dateTimeSent).getTime());



			}

			if (sortby.direction == "desc") {


				this.CorporatePartnerList = this.CorporatePartnerList.sort((a, b) => new Date(a.dateTimeSent).getTime() - new Date(b.dateTimeSent).getTime());

				//this.CorporatePartnerList = this.CorporatePartnerList .sort((a: any, b: any) => b.title-a.title);

			}
		}




	}

	searching(e) {

		if (this.search.length == 0) {

			this.CorporatePartnerList = this.duplicate;
		}
		else {
			this.CorporatePartnerList = this.duplicate.filter(x => { return x.title.toLocaleLowerCase().includes(this.search) || x.description.toLocaleLowerCase().includes(this.search) || x.userEmail.toString().toLocaleLowerCase().includes(this.search) })

		}


	}
}
