import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporatePartnerComponent } from './corporate-partner.component';

describe('CorporatePartnerComponent', () => {
  let component: CorporatePartnerComponent;
  let fixture: ComponentFixture<CorporatePartnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporatePartnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporatePartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
