import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { ActionComponent } from './header/action/action.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ProfileComponent } from './header/profile/profile.component';
import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { InnerComponent } from "./components/inner/inner.component";
import { TrainingComponent } from './components/training/listTraining/training.component';
import { AddTrainingComponent } from './components/training/add-training/add-training.component';
import { DetailTrainingComponent } from './components/training/detail-training/detail-training.component';
import { AuthGuard } from '../guards/auth.guard';
import { TrainedParticipantByCNICnameComponent } from './components/Reports/trained-participant-by-cnicname/trained-participant-by-cnicname.component';
import { TrainingSummarybydistrictComponent } from './components/Reports/training-summarybydistrict/training-summarybydistrict.component';
import { TrainingdetailreportbyfacilityComponent } from './components/Reports/trainingdetailreportbyfacility/trainingdetailreportbyfacility.component';
import { HomeComponent } from './components/home/home.component';
import { ProductComponent } from './components/product/product.component';
import { ProductModalComponent } from './components/product/product-modal/product-modal.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { CorporatePartnerComponent } from './components/corporate-partner/corporate-partner.component';
import { CorporatePartnerModalComponent } from './components/corporate-partner/corporate-partner-modal/corporate-partner-modal.component';



const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: '',
		component: PagesComponent,
		
		// Remove comment to enable login
		canActivate: [AuthGuard],
		data: {
			permissions: {
				only: ['ADMIN', 'USER'],
				except: ['GUEST'],
				redirectTo: '/'
			}
		},
		children: [
			
			{
				path: 'builder',
				loadChildren: './builder/builder.module#BuilderModule'
			},
			{
				path: 'header/actions',
				component: ActionComponent
			},
			{
				path: 'CorporatePartner',
				component: CorporatePartnerComponent
			},
			
			{
				path: 'profile',
				component: ProfileComponent
			},
			{
				path: 'inner',
				component: InnerComponent
			},
			{
				path: 'notification',
				component: TrainingComponent
			},
			{
				path: 'trainings/addNew',
				component: AddTrainingComponent
			},
			{
				path: 'trainings/addNew/:id',
				component: AddTrainingComponent
			},
			{
				path: 'ct',
				component: CorporatePartnerModalComponent
			},
			{
				path: 'trainings/details/:id',
				component: DetailTrainingComponent
			},
			
			{
				path: 'report/SearchByCNICName',
				component: TrainedParticipantByCNICnameComponent
			},
			{
				path: 'report/SearchByDistict',
				component: TrainingSummarybydistrictComponent
			},
			{
				path: 'report/SearchByFacility',
				component: TrainingdetailreportbyfacilityComponent
			},
			{
				path: 'product',
				component: ProductComponent
			},
			{
				path: 'editProduct',
				component: ProductModalComponent
			},
			{
				path: 'dashboard',
				component: AnalyticsComponent
			},
		
			
			
			
							
				]
	},
	{
		path: 'login',
		// canActivate: [NgxPermissionsGuard],
		loadChildren: './auth/auth.module#AuthModule',
		data: {
			permissions: {
				except: 'ADMIN'
			}
		},
	},
	{
		path: '404',
		component: ErrorPageComponent
	},
	{
		path: 'error/:type',
		component: ErrorPageComponent
	},
	
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
