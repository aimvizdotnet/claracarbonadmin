import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from "../../../../environments/environment";
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  url: string = environment.server;
  constructor(private http: HttpClient, private _http: HttpClient,private router: Router,) { }

  login(Username: string, password: string) {
    
    let obj = { Email: Username, Password: password };
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });  
    return this.http.post<any>(this.url + 'CCAdminLogin',JSON.stringify (obj))
      .pipe(map((user:any) => {
        var data = user;
      if(user.isSuccess){

        
     
    
      
      //this.router.navigate(['/dashboard']);
        if (data) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(data));
          this.router.navigate(['/dashboard']);
        }
      }  
        return data;
      }));
  }
  loginAs(EmailAddress: string, password: string) {
    return this.http.post<any>(this.url + 'users/authenticateAs', { EmailAddress, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  getUser() {
    var user = localStorage.getItem('currentUser');
    if (user == null || user == '') {
      return null;
    }
    else {
      return JSON.parse(user);
    }
  }

  FullName = function () {
    var user = localStorage.getItem('currentUser');
    if (user == null || user == '') {
      return null;
    }
    else {
      var userJSON = JSON.parse(user);
      return userJSON.FirstName + ' ' + userJSON.LastName;
    }
  }

  EmailAddress = function () {
    var user = localStorage.getItem('currentUser');
    if (user == null || user == '') {
      return null;
    }
    else {
      var userJSON = JSON.parse(user);
      return userJSON.EmailAddress;
    }
  }

  // user management work
  GetAllCompanyList() {
    return this._http.post(this.url + "users/GetAllCompanyList", null);
  }

  register(data) {
    return this._http.post(this.url + "users/Register", data);
  }

  RegisterInvitedUser(data): any {
    return this._http.post(this.url + "users/RegisterInvitedUser", data);
  }

  verifyUser(uc) {
    return this._http.post(this.url + "users/VerifyUser/" + uc, uc);
  }

  getUniqueCompany(user): any {
    return this._http.post(this.url + "users/GetUniqueCompany/", user);
  }

  SetPassword(user): Observable<any> {
    return this._http.post(this.url + "users/SetPassword", user);
  }
  checkSubscriptionExpiry(): boolean {
    var user = JSON.parse(localStorage.getItem('currentUser'));
    if (!user) {
      return false;
    }
    else {

      return (new Date(user.BraintreeSubscriptionExpiryDate)) >= (new Date());
    }
    //return false;
  }
}
