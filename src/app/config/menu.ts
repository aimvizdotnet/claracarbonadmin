// tslint:disable-next-line:no-shadowed-variable
import { ConfigModel } from '../core/interfaces/config';

// tslint:disable-next-line:no-shadowed-variable
export class MenuConfig implements ConfigModel {
	public config: any = {};

	constructor() {

		let user = JSON.parse(localStorage.getItem('currentUser'));
		let aside: any;
		
		this.config = {
			header: {
				self: {},
				items: [
					{
						title: 'Reports',
						root: true,
						icon: 'flaticon-line-graph',
						toggle: 'click',
						translate: 'MENU.REPORTS',
						submenu: {
							type: 'mega',
							width: '1000px',
							alignment: 'left',
							columns: [
								{
									heading: {
										heading: true,
										title: 'Finance Reports',
									},
									items: [
										{
											title: 'Annual Reports',
											tooltip: 'Non functional dummy link',
											icon: 'flaticon-map',
										},
										{
											title: 'HR Reports',
											tooltip: 'Non functional dummy link',
											icon: 'flaticon-user',
										},
										{
											title: 'IPO Reports',
											tooltip: 'Non functional dummy link',
											icon: 'flaticon-clipboard',
										},
										{
											title: 'Finance Margins',
											tooltip: 'Non functional dummy link',
											icon: 'flaticon-graphic-1',
										},
										{
											title: 'Revenue Reports',
											tooltip: 'Non functional dummy link',
											icon: 'flaticon-graphic-2',
										}
									]
								},
								{
									bullet: 'line',
									heading: {
										heading: true,
										title: 'Project Reports',
									},
									items: [
										{
											title: 'Coca Cola CRM',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title:
												'Delta Airlines Booking Site',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Malibu Accounting',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Vineseed Website Rewamp',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Zircon Mobile App',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Mercury CMS',
											tooltip: 'Non functional dummy link',
											icon: '',
										}
									]
								},
								{
									bullet: 'dot',
									heading: {
										heading: true,
										title: 'HR Reports',
									},
									items: [
										{
											title: 'Staff Directory',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Client Directory',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Salary Reports',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Staff Payslips',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Corporate Expenses',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Project Expenses',
											tooltip: 'Non functional dummy link',
											icon: '',
										}
									]
								},
								{
									heading: {
										heading: true,
										title: 'Reporting Apps',
										icon: '',
									},
									items: [
										{
											title: 'Report Adjusments',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Sources & Mediums',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Reporting Settings',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Conversions',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Report Flows',
											tooltip: 'Non functional dummy link',
											icon: '',
										},
										{
											title: 'Audit & Logs',
											tooltip: 'Non functional dummy link',
											icon: '',
										}
									]
								}
							]
						}
					}
				]
			},
			aside: aside,
			
			
			//role for clara carbon for side menu(hamburger)
			CC_MENU : {
				self: {},
				items: [
					// {
					// 	title: 'Dashboard',
					// 	desc: 'Latest updates and statistic charts',
					// 	root: true,
					// 	icon: 'fas fa-chart-bar',
					// 	page: '/dashboard',
					// 	badge: { type: 'm-badge--danger', value: '2' },
					// 	translate: 'MENU.DASHBOARD'
					// },
					{
						title: 'Dashboard',
						desc: 'Latest updates and statistic charts',
						root: true,
						icon: 'fas fa-chart-bar',
						page: '/dashboard',
					},
					{
						title: 'Notification',
						desc: 'Some description goes here',
						root: true,
						icon: 'flaticon-line-graph',
						page: '/notification'
					},
					{
						title: 'Products',
						desc: 'Some description goes here',
						root: true,
						icon: 'flaticon-line-graph',
						page: '/product'
					},
					{
						title: 'Corporate Partner',
						desc: 'Some description goes here',
						root: true,
						icon: 'flaticon-line-graph',
						page: '/CorporatePartner'
					},
					
					// {
					// 	title: 'Nominations',
					// 	desc: 'Some description goes here',
					// 	root: true,
					// 	icon: 'fas fa-clipboard-list',
					// 	page: '/nominations'
					// },
					// {

					// 	title: 'Trainers',
					// 	desc: 'Trainer description',
					// 	root: true,
					// 	icon: 'fas fa-user',
					// 	page: '/trainers',
					// },
					// {
					// 	title: 'Participants',
					// 	desc: 'Some description goes here',
					// 	root: true,
					// 	icon: 'fas fa-users',
					// 	page: '/participants'
					// },
					// {
					// 	title: 'Users',
					// 	desc: 'Some description goes here',
					// 	root: true,
					// 	icon: 'fas fa-users',
					// 	page: '/users'
					// },
					// {
					// 	title: 'Training Manuals',
					// 	desc: 'Training module detail',
					// 	root: true,
					// 	icon: 'fas fa-users',
					// 	page: '/trainingmodule'
					// },
					// {
					// 	title: 'Setup',
					// 	root: true,
					// 	icon: 'fas fa-dolly',
					// 	submenu:
					// 		[
					// 			{
					// 				title: 'Cadres',
					// 				root: true,
					// 				icon: 'fas fa-clipboard-list',
					// 				page: '/cadres',
					// 			},
					// 			{
					// 				title: 'Job Titles',
					// 				root: true,
					// 				icon: 'fas fa-plus-circle',
					// 				page: '/jobtitles'
					// 			}, {
					// 				title: 'Qualifications',
					// 				root: true,
					// 				icon: 'fas fa-upload',
					// 				page: '/qualifications'
					// 			},
					// 			{
					// 				title: 'Serving Levels',
					// 				root: true,
					// 				icon: 'fas fa-search-plus',
					// 				page: '/servinglevels'
					// 			},
					// 			{
					// 				title: 'Organizations',
					// 				root: true,
					// 				icon: 'fas fa-search-plus',
					// 				page: '/trainingorganizations'
					// 			},
					// 			{
					// 				title: 'Type of Trainees',
					// 				root: true,
					// 				icon: 'fas fa-search-plus',
					// 				page: '/typeoftrainees'
					// 			},
					// 			{
					// 				title: 'Type of Trainings',
					// 				root: true,
					// 				icon: 'fas fa-search-plus',
					// 				page: '/typeoftrainings'
					// 			},{
					// 				title: 'Facility Type',
					// 				root: true,
					// 				icon: 'fas fa-search-plus',
					// 				page: '/FacilityTpe'
					// 			},
					// 		]
					// },
				]
			},
			
			
		};
	}
	
}
